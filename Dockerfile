FROM openjdk:14-jdk AS app-build

ENV GRADLE_OPTS -Dorg.gradle.daemon=false
COPY ./dto /build/dto
COPY ./Server /build/Server
COPY ./rest-client /build/rest-client
WORKDIR /build
RUN cd ./dto && ./gradlew install && cd ../Server && ./gradlew build && cd .. && cd ./rest-client && ./gradlew build && cd .. 