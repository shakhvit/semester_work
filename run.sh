#!/bin/bash

if [ "$#" -ne 1 ]; then
    echo "Wrong number of arguments"
    exit 1
fi

SERVER="--server"
HELP="--help"
CLIENT="--client"

if [ "$1" = "$SERVER" ]; then
    cd ./Server/ && \
	java -jar build/libs/semester_work_server-1.0-SNAPSHOT.jar
elif [ "$1" = "$CLIENT" ]; then
    cd ./rest-client && \
	java -jar build/libs/rest-client-1.0-SNAPSHOT.jar
elif [[ "$1" == "$HELP" ]]; then
    echo "Run with argument --server to run server, with argument --client to run client"
fi
