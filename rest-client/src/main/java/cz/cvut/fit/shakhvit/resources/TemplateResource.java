package cz.cvut.fit.shakhvit.resources;

import org.springframework.hateoas.PagedModel;

import java.net.URI;

public interface TemplateResource<TypeCreateDTO,TypeDTO> {

    public URI create(TypeCreateDTO entity);

    public PagedModel<TypeDTO> readPage(int page, int size);

    public TypeDTO readById(int id);

    public TypeDTO update(int id, TypeCreateDTO data);

    public void deleteById(int id);
}
