package cz.cvut.fit.shakhvit.resources;


import cz.cvut.fit.shakhvit.dto.DanceSchoolCreateDTO;
import cz.cvut.fit.shakhvit.dto.DanceSchoolDTO;
import cz.cvut.fit.shakhvit.exception.MyRestClientException;
import cz.cvut.fit.shakhvit.exception.RestTemplateErrorHandling;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

@Component
public class DanceSchoolResource implements TemplateResource<DanceSchoolCreateDTO, DanceSchoolDTO> {

    private final RestTemplate restTemplate;

    public DanceSchoolResource(RestTemplateBuilder restTemplateBuilder) {
        restTemplate = restTemplateBuilder.rootUri(ROOT_RESOURCE_URL).errorHandler(new RestTemplateErrorHandling()).build();
    }

    private static final String COLLECTION_PAGED_URL = "/?page={page}&size={size}";
    private static final String ROOT_RESOURCE_URL = "http://localhost:8080/api/v1/schools";
    private static final String ONE_URI = "/{id}";

    public URI create(DanceSchoolCreateDTO data) {
        return restTemplate.postForLocation("/", data);
    }

    public DanceSchoolDTO update(int id, DanceSchoolCreateDTO data ){
        HttpEntity<DanceSchoolCreateDTO> entity = new HttpEntity<>(data);
        restTemplate.exchange(
                ONE_URI, HttpMethod.PUT, entity, DanceSchoolCreateDTO.class, id
        );
        return readById(id);
    }

    public DanceSchoolDTO readById(int id) {
        return restTemplate.getForObject(ONE_URI, DanceSchoolDTO.class, id);
    }

    public PagedModel<DanceSchoolDTO> readPage(int page, int size) {
        ResponseEntity<PagedModel<DanceSchoolDTO>> result = restTemplate.exchange(
                COLLECTION_PAGED_URL, HttpMethod.GET, null,
                new ParameterizedTypeReference<PagedModel<DanceSchoolDTO>>(){},
                page, size
        );
        return result.getBody();
    }

    public void deleteById(int id) {
        restTemplate.delete(ONE_URI, id);
    }

    public DanceSchoolDTO addDancer(int id, int dancerId) {
        ResponseEntity<DanceSchoolDTO> result = restTemplate.exchange(
                ONE_URI+"/addDancer?dancerId={dancerId}",
                HttpMethod.PUT, null,
                new ParameterizedTypeReference<DanceSchoolDTO>() {},
                id, dancerId
        );
        return result.getBody();
    }

    public DanceSchoolDTO addTrainer(int id, int trainerId) {
        ResponseEntity<DanceSchoolDTO> result = restTemplate.exchange(
                ONE_URI+"/addTrainer?trainerId={trainerId}",
                HttpMethod.PUT, null,
                new ParameterizedTypeReference<DanceSchoolDTO>() {},
                id, trainerId
        );
        return result.getBody();
    }

    public DanceSchoolDTO findByName(String name){
        return restTemplate.getForObject("/find?name={name}", DanceSchoolDTO.class, name);
    }

}
