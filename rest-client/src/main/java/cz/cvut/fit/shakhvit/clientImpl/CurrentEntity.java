package cz.cvut.fit.shakhvit.clientImpl;

public enum CurrentEntity {
    DANCER, TRAINER, DANCE_SCHOOL;

    @Override
    public String toString() {
        if(this == DANCER) return "dancer";
        else if(this == TRAINER) return "trainer";
        else return "dance school";
    }
}
