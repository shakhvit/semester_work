package cz.cvut.fit.shakhvit.clientImpl;

import cz.cvut.fit.shakhvit.dto.DanceSchoolDTO;
import cz.cvut.fit.shakhvit.dto.DancerDTO;
import cz.cvut.fit.shakhvit.dto.TrainerDTO;
import cz.cvut.fit.shakhvit.helpers.Helpers;
import cz.cvut.fit.shakhvit.resources.DanceSchoolResource;
import cz.cvut.fit.shakhvit.resources.DancerResource;
import cz.cvut.fit.shakhvit.resources.TrainerResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Console;
import java.lang.reflect.Type;
import java.util.InputMismatchException;
import java.util.Scanner;


public class ClientImpl {
    
    public final Helpers helpers = new Helpers();

    private DancerResource dancerResource;

    private TrainerResource trainerResource;

    private DanceSchoolResource danceSchoolResource;

    public Scanner scan = new Scanner(System.in);
    public Console cnsl = System.console();

    public CurrentEntity curEntity = CurrentEntity.DANCER;

    public ClientImpl(DanceSchoolResource danceSchoolResource, DancerResource dancerResource, TrainerResource trainerResource) {
        this.dancerResource = dancerResource;
        this.trainerResource = trainerResource;
        this.danceSchoolResource = danceSchoolResource;
    }


    public final  String exit = "\\exit";
    public final  String commands = "\\commands";
    public final  String update = "\\update";
    public final  String read = "\\read";
    public final  String choose = "\\choose";
    public final  String create = "\\create";
    public final  String help = "\\help";
    public final  String delete = "\\delete";
    public final  String addTrainer = "\\addTrainer";
    public final  String addDancer = "\\addDancer";
    public final  String changeSchool = "\\changeSchool";
    public final  String addSchool = "\\addSchool";
    public final  String getPotentialDancers = "\\getPotentialDancers";
    public final  String findByName = "\\findByName";

    public final String INFO = "Helpful commands:\n"+
            help+" - for help\n"+
            choose+" - to choose another entity\n"+
            create+" - to create new instance of object\n"+
            read+" - to read from database\n"+
            update+" - to update\n"+
            delete+" - to delete\n"+
            commands+" - to see set of commands\n"+
            exit+" - to exit\n";

    public String entityToString() {
        return curEntity.toString();
    }


    public void createImpl() {
        if(curEntity==CurrentEntity.DANCER) dancerResource.create(helpers.readDancerCreateDTO(scan, cnsl));
        else if(curEntity==CurrentEntity.TRAINER) trainerResource.create(helpers.readTrainerCreateDTO(scan, cnsl));
        else danceSchoolResource.create(helpers.readDanceSchoolCreateDTO(scan, cnsl));
    }

    public void setCurEntity(int n) {
        if(n==1) curEntity = CurrentEntity.DANCER;
        else if(n==2) curEntity = CurrentEntity.TRAINER;
        else curEntity = CurrentEntity.DANCE_SCHOOL;
    }

    public String getPossibleCommands() {
        String res = create + ", " + read + ", " + update + ", " + delete;
        if(curEntity==CurrentEntity.DANCER) res += ", " + addTrainer + ", " + changeSchool;
        if(curEntity==CurrentEntity.TRAINER) res += ", " + addDancer + ", " + addSchool + ", " + getPotentialDancers;
        if(curEntity==CurrentEntity.DANCE_SCHOOL) res += ", " + addDancer + ", " + addTrainer +", " + findByName;
        return res;
    }

    public void deleteImpl() {
        System.out.print("Enter id: ");
        int id = scan.nextInt();
        if(curEntity==CurrentEntity.DANCER) helpers.deleteByIdHelper(dancerResource, id);
        else if(curEntity==CurrentEntity.TRAINER) helpers.deleteByIdHelper(trainerResource, id);
        else helpers.deleteByIdHelper(danceSchoolResource, id);
    }

    public void updateImpl() {
        System.out.print("Enter id: ");
        int id;
        try {
            id = scan.nextInt();
        } catch( InputMismatchException e ){
            System.out.println("Number expected");
            return;
        }
        if(curEntity==CurrentEntity.DANCER) dancerResource.update(id, helpers.readDancerCreateDTO(scan, cnsl));
        else if(curEntity==CurrentEntity.TRAINER) trainerResource.update(id, helpers.readTrainerCreateDTO(scan, cnsl));
        else danceSchoolResource.update(id, helpers.readDanceSchoolCreateDTO(scan, cnsl));
    }

    public void readImpl() {
        System.out.println("Enter 1, if you want to read one by id, 2, if you want read all");
        int num;
        try {
            num = scan.nextInt();
            if(num!=1 && num != 2) {
                System.out.println("Wrong number");
                return;
            }
        } catch(InputMismatchException e) {
            System.out.println("Number expected");
            return;
        }
        if(num==2){
            if(curEntity==CurrentEntity.DANCER)
                helpers.printAllPageHelper(dancerResource);
            else if(curEntity==CurrentEntity.TRAINER)
                helpers.printAllPageHelper(trainerResource);
            else
                helpers.printAllPageHelper(danceSchoolResource);
            return;
        }

        System.out.print("Enter id:");
        try{
            num = scan.nextInt();
        } catch(InputMismatchException e){
            System.out.println("Number expected");
        }
        if(curEntity==CurrentEntity.DANCER) {
            DancerDTO result = dancerResource.readById(num);
            System.out.println(result.toString());
            return;
        }
        else if(curEntity==CurrentEntity.TRAINER) {
            TrainerDTO result = trainerResource.readById(num);
            System.out.println(result.toString());
            return;
        }
        else if(curEntity==CurrentEntity.DANCE_SCHOOL){
            DanceSchoolDTO result = danceSchoolResource.readById(num);
            System.out.println(result.toString());
            return;
        }

    }

    public void addDancerImpl() {
        if(curEntity == CurrentEntity.DANCER) {
            System.err.println("Entity dancer does not support command \\addDancer");
            infoMsg();
            return;
        }
        if(curEntity == CurrentEntity.TRAINER){
            System.out.print("Enter trainer id: ");
            int trainerId;
            try{
                trainerId = scan.nextInt();
            } catch( InputMismatchException e ) {
                System.err.println("Number expected");
                return;
            }
            System.out.print("Enter dancer id: ");
            int dancerId;
            try {
                dancerId = scan.nextInt();
            } catch(InputMismatchException e){
                System.err.println("Number expected");
                return;
            }
            trainerResource.addDancer(trainerId, dancerId);
        }
        else {
            System.out.print("Enter dance school id: ");
            int danceSchoolId;
            try{
                danceSchoolId = scan.nextInt();
            } catch( InputMismatchException e ) {
                System.err.println("Number expected");
                return;
            }
            System.out.print("Enter dancer id: ");
            int dancerId;
            try {
                dancerId = scan.nextInt();
            } catch(InputMismatchException e){
                System.err.println("Number expected");
                return;
            }
            danceSchoolResource.addDancer(danceSchoolId, dancerId);
        }
    }

    public void addTrainerImpl() {
        if(curEntity == CurrentEntity.TRAINER ) {
            System.err.println("Entity trainer does not support \\addTrainer command");
            infoMsg();
            return;
        }
        if(curEntity == CurrentEntity.DANCER){
            System.out.print("Enter dancer id: ");
            int dancerId;
            try{
                dancerId = scan.nextInt();
            } catch( InputMismatchException e ) {
                System.err.println("Number expected");
                return;
            }
            System.out.print("Enter dancer id: ");
            int trainerId;
            try {
                trainerId = scan.nextInt();
            } catch(InputMismatchException e){
                System.err.println("Number expected");
                return;
            }
            dancerResource.addTrainer(trainerId, trainerId);
        }
        else {
            System.out.print("Enter dance school id: ");
            int danceSchoolId;
            try{
                danceSchoolId = scan.nextInt();
            } catch( InputMismatchException e ) {
                System.err.println("Number expected");
                return;
            }
            System.out.print("Enter dancer id: ");
            int trainerId;
            try {
                trainerId = scan.nextInt();
            } catch(InputMismatchException e){
                System.err.println("Number expected");
                return;
            }
            danceSchoolResource.addTrainer(danceSchoolId, trainerId);
        }
    }

    public void addSchoolsImpl() {
        if(curEntity == CurrentEntity.DANCE_SCHOOL ) {
            System.err.println("Entity dance school does not support \\addSchool command");
            infoMsg();
            return;
        }
        if(curEntity == CurrentEntity.DANCER ) {
            System.err.println("Entity dancer does not support \\addSchool command");
            System.err.println("Use \\changeSchool instead");
            infoMsg();
            return;
        }
        System.out.print("Enter trainer id: ");
        int trainerID;
        try{
            trainerID = scan.nextInt();
        } catch( InputMismatchException e ) {
            System.err.println("Number expected");
            return;
        }
        System.out.print("Enter dance school id: ");
        int schoolId;
        try {
            schoolId = scan.nextInt();
        } catch(InputMismatchException e){
            System.err.println("Number expected");
            return;
        }
        trainerResource.addDanceSchool(trainerID, schoolId);
    }

    public void changeSchoolImpl() {
        if(curEntity == CurrentEntity.TRAINER) {
            System.err.println("Entity trainer does not support \\changeSchool command");
            System.err.println("Use \\addSchool instead");
            infoMsg();
            return;
        }
        if(curEntity == CurrentEntity.DANCE_SCHOOL) {
            System.err.println("Entity dance school does not support \\changeSchool command");
            infoMsg();
            return;
        }
        System.out.print("Enter dancer id: ");
        int dancerId;
        try{
            dancerId = scan.nextInt();
        } catch( InputMismatchException e ) {
            System.err.println("Number expected");
            return;
        }
        System.out.print("Enter dance school id: ");
        int schoolId;
        try {
            schoolId = scan.nextInt();
        } catch(InputMismatchException e){
            System.err.println("Number expected");
            return;
        }
        dancerResource.changeDanceSchool(dancerId, schoolId);
    }

    public void getPotentialDancersImpl() {
        if(curEntity == CurrentEntity.DANCER){
            System.err.println("Entity dancer does not support \\getPotentialDancers command");
            infoMsg();
            return;
        }
        if(curEntity == CurrentEntity.DANCE_SCHOOL) {
            System.err.println("Entity dance school does not support \\getPotentialDancers command");
            infoMsg();
            return;
        }
        System.out.print("Enter trainer id: ");
        int trainerID;
        try{
            trainerID = scan.nextInt();
        } catch( InputMismatchException e ) {
            System.err.println("Number expected");
            return;
        }
        helpers.PrintPotentialDancers(trainerResource, trainerID);
    }

    public void findByNameImpl() {
        if(curEntity==CurrentEntity.DANCER||curEntity==CurrentEntity.TRAINER) {
            System.err.println("Entity " + curEntity.toString() +" does not support \\findByName command");
            infoMsg();
            return;
        }
        String name = cnsl.readLine("Enter dance school name: ");
        System.out.println(danceSchoolResource.findByName(name).toString());
    }

    public void infoMsg() {
        System.out.println("Type \\help if you need help");
    }

    
}
