package cz.cvut.fit.shakhvit.helpers;

import cz.cvut.fit.shakhvit.dto.DanceSchoolCreateDTO;
import cz.cvut.fit.shakhvit.dto.DancerCreateDTO;
import cz.cvut.fit.shakhvit.dto.DancerDTO;
import cz.cvut.fit.shakhvit.dto.TrainerCreateDTO;
import cz.cvut.fit.shakhvit.resources.TemplateResource;
import cz.cvut.fit.shakhvit.resources.TrainerResource;
import org.springframework.hateoas.PagedModel;

import java.io.Console;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Helpers {

    public  DancerCreateDTO readDancerCreateDTO(Scanner scan, Console cnsl) {
        String firstName = cnsl.readLine("Enter first name: ");
        String lastName = cnsl.readLine("Enter last name: ");
        System.out.print("Enter ID of dance school or 0: ");
        int id = scan.nextInt();
        System.out.print("Enter IDs of trainers ending with 0: ");
        List<Integer> trainerIds = new ArrayList<>();
        while(true) {
            int next = scan.nextInt();
            if(next == 0) break;
            trainerIds.add(next);
        }
        return new DancerCreateDTO(firstName, lastName,
            id == 0 ? null : id,
            trainerIds.size() == 0 ? null : trainerIds);
    }

    public  TrainerCreateDTO readTrainerCreateDTO(Scanner scan, Console cnsl){
        String firstName = cnsl.readLine("Enter first name: ");
        String lastName = cnsl.readLine("Enter last name: ");
        System.out.print("Enter IDs of dancers ending with 0: ");
        List<Integer> dancersIds = new ArrayList<>();
        while(true) {
            int next = scan.nextInt();
            if(next == 0) break;
            dancersIds.add(next);
        }
        System.out.print("Enter IDs of dance schools ending with 0: ");
        List<Integer> schoolsIds = new ArrayList<>();
        while(true) {
            int next = scan.nextInt();
            if(next == 0) break;
            schoolsIds.add(next);
        }
        return new TrainerCreateDTO(firstName, lastName,
                schoolsIds.size() == 0 ? null : schoolsIds,
                dancersIds.size() == 0 ? null : dancersIds );
    }

    public  DanceSchoolCreateDTO readDanceSchoolCreateDTO(Scanner scan, Console cnsl) {
        String name = cnsl.readLine("Enter name of dance school: ");
        System.out.print("Enter number of halls: ");
        int num = scan.nextInt();
        System.out.print("Enter IDs of trainers ending with 0: ");
        List<Integer> trainerIds = new ArrayList<>();
        while(true) {
            int next = scan.nextInt();
            if(next == 0) break;
            trainerIds.add(next);
        }
        System.out.print("Enter IDs of dancers ending with 0: ");
        List<Integer> dancersIds = new ArrayList<>();
        while(true) {
            int next = scan.nextInt();
            if(next == 0) break;
            dancersIds.add(next);
        }
        return new DanceSchoolCreateDTO(
                name, num,
                dancersIds.size() == 0 ? null : dancersIds,
                trainerIds.size() == 0 ? null : trainerIds
        );
    }



    public  <Resource extends TemplateResource> void deleteByIdHelper(Resource resource, int id) {
        resource.deleteById(id);
    }

    public  <TypeDTO, Resource extends TemplateResource> PagedModel<TypeDTO>
    readPageHelper(Resource resource, int page, int size) {
        return resource.readPage(page, size);
    }

    public  <TypeDTO, Resource extends TemplateResource> void
    printAllPageHelper(Resource resource, int size) {
        int page = 0;
        PagedModel<TypeDTO> pages;
        do{
            pages = readPageHelper(resource, page, size);
            for(var e : pages)
                System.out.println(e.toString());
            page ++;
        } while(pages.getNextLink().isPresent());
    }

    public  <TypeDTO, Resource extends TemplateResource> void
    printAllPageHelper(Resource resource) {
        printAllPageHelper(resource, 10);
    }

    public void PrintPotentialDancers(TrainerResource trainerResource, int id, int size){
        int page = 0;
        PagedModel<DancerDTO> pages;
        do{
            pages = trainerResource.getPotentialDancers(id, page, size);
            for(var e : pages)
                System.out.println(e.toString());
            page ++;
        } while(pages.getNextLink().isPresent());
    }

    public void PrintPotentialDancers(TrainerResource trainerResource, int id){
        PrintPotentialDancers(trainerResource, id, 10);
    }

    public  <TypeDTO, Resource extends TemplateResource> TypeDTO readOneHelper(Resource resource, int id) {
        return (TypeDTO) resource.readById(id);
    }
}
