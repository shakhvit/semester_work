package cz.cvut.fit.shakhvit.exception;

public class BodyToJson {
    public String timestamp;
    public int status;
    public String error;
    public String message;
    public String path;
}
