package cz.cvut.fit.shakhvit.exception;


import com.google.gson.Gson;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.stream.Collectors;


public class RestTemplateErrorHandling extends DefaultResponseErrorHandler {

    @Override
    public void handleError(ClientHttpResponse response) throws IOException {
        if(response.getStatusCode().is4xxClientError() || response.getStatusCode().is5xxServerError()) {
            try(BufferedReader reader = new BufferedReader(new InputStreamReader(response.getBody()))){
                String msg = reader.lines().collect(Collectors.joining(""));
                Gson g = new Gson();
                BodyToJson b = g.fromJson(msg, BodyToJson.class);
                throw new MyRestClientException(response.getStatusCode(), b.message);
            }
        }
    }
}
