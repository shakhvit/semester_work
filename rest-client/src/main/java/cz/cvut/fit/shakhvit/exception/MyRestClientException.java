package cz.cvut.fit.shakhvit.exception;

import org.springframework.http.HttpStatus;

public class MyRestClientException extends RuntimeException {

    private HttpStatus status;
    private String msg;

    public MyRestClientException(HttpStatus status, String msg) {
        this.status = status;
        this.msg = msg;
    }

    public String toString() {
        return this.status.toString() + " with message: " + this.msg;
    }

    public String getMsg() {
        return this.msg;
    }

    public int getStatus(){
        return this.status.value();
    }
}
