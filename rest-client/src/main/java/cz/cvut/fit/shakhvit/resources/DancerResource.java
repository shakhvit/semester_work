package cz.cvut.fit.shakhvit.resources;


import cz.cvut.fit.shakhvit.dto.DancerCreateDTO;
import cz.cvut.fit.shakhvit.dto.DancerDTO;
import cz.cvut.fit.shakhvit.exception.RestTemplateErrorHandling;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

@Component
public class DancerResource implements TemplateResource<DancerCreateDTO, DancerDTO> {
    private final RestTemplate restTemplate;

    public DancerResource(RestTemplateBuilder restTemplateBuilder) {
        restTemplate = restTemplateBuilder.rootUri(ROOT_RESOURCE_URL).errorHandler(new RestTemplateErrorHandling()).build();
    }

    private static final String COLLECTION_PAGED_URL = "/?page={page}&size={size}";
    private static final String ROOT_RESOURCE_URL = "http://localhost:8080/api/v1/dancers";
    private static final String ONE_URI = "/{id}";


    public URI create(DancerCreateDTO data) {
        return restTemplate.postForLocation("/", data );
    }

    public DancerDTO readById(int id) {
        return restTemplate.getForObject(ONE_URI, DancerDTO.class, id);
    }

    public PagedModel<DancerDTO> readPage(int page, int size) {
        ResponseEntity<PagedModel<DancerDTO>> result = restTemplate.exchange(COLLECTION_PAGED_URL,
                HttpMethod.GET, null,
                new ParameterizedTypeReference<PagedModel<DancerDTO>>(){}, page, size);
        return result.getBody();
    }

    public DancerDTO update(int id, DancerCreateDTO data) {
        HttpEntity<DancerCreateDTO> entity = new HttpEntity<>(data);
        restTemplate.exchange(
                ONE_URI,
                HttpMethod.PUT,
                entity, DancerCreateDTO.class, id);
        return readById(id);
    }

    public DancerDTO changeDanceSchool(int id, int schoolId ) {
        ResponseEntity<DancerDTO> result = restTemplate.exchange(
                ONE_URI + "/changeSchool?schoolId={schoolId}",
                HttpMethod.PUT, null,
                new ParameterizedTypeReference<DancerDTO>() {}, id, schoolId );
        return result.getBody();
    }

    public DancerDTO addTrainer(int id, int trainerId) {
        ResponseEntity<DancerDTO> result = restTemplate.exchange(
                ONE_URI + "/addTrainer?trainerId={trainerId}",
                HttpMethod.PUT, null,
                new ParameterizedTypeReference<DancerDTO>() {}, id, trainerId);
        return result.getBody();
    }

    public void deleteById(int id) {
        restTemplate.delete(ONE_URI, id);
    }
}
