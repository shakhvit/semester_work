package cz.cvut.fit.shakhvit.restclient;

import cz.cvut.fit.shakhvit.clientImpl.ClientImpl;
import cz.cvut.fit.shakhvit.dto.*;
import cz.cvut.fit.shakhvit.exception.MyRestClientException;
import cz.cvut.fit.shakhvit.helpers.Helpers;
import cz.cvut.fit.shakhvit.resources.DanceSchoolResource;
import cz.cvut.fit.shakhvit.resources.DancerResource;
import cz.cvut.fit.shakhvit.resources.TrainerResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.hateoas.config.EnableHypermediaSupport;
import org.springframework.hateoas.config.HypermediaRestTemplateConfigurer;

import java.io.Console;
import java.lang.reflect.Type;
import java.util.*;

@SpringBootApplication
@EnableHypermediaSupport(type = EnableHypermediaSupport.HypermediaType.HAL)
@ComponentScan("cz.cvut.fit.shakhvit.resources")
public class RestClientApplication implements ApplicationRunner {


    private DancerResource dancerResource;

    private TrainerResource trainerResource;

    private DanceSchoolResource danceSchoolResource;

    private ClientImpl client;

    @Autowired
    public void setDancerResource(DancerResource dancerResource) {
        this.dancerResource = dancerResource;
    }

    @Autowired
    public void setTrainerResource(TrainerResource trainerResource) {
        this.trainerResource = trainerResource;
    }

    @Autowired
    public void setDanceSchoolResource(DanceSchoolResource danceSchoolResource) {
        this.danceSchoolResource = danceSchoolResource;
    }

    @Autowired
    public void setClient() {
        this.client = new ClientImpl(this.danceSchoolResource, this.dancerResource, this.trainerResource);
    }



    public static void main(String[] args) {
        SpringApplication.run(RestClientApplication.class, args);
    }

    @Bean
    RestTemplateCustomizer customizer(HypermediaRestTemplateConfigurer c){
        return restTemplate -> {c.registerHypermediaTypes(restTemplate);};
    }


    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println("Welcome!");
        System.out.println("Type \\help for help");
        while(true) {
            try {

                String input = client.cnsl.readLine("$("+client.entityToString()+") ");

                if(input.equals(client.exit)) break;

                if(input.equals(client.help)) {
                    System.out.println(client.INFO);
                    continue;
                }

                if(input.equals(client.choose)) {
                    System.out.println("Enter number of entity:\n1 - Dancer\n2 - Trainer\n3 - DanceSchool");
                    try{
                        int num = client.scan.nextInt();
                        if(num!=1&&num!=2&&num!=3) {
                            System.out.println("Wrong number");
                            continue;
                        }
                        client.setCurEntity(num);
                        continue;
                    } catch(InputMismatchException e) {
                        System.out.println("Number expected");
                        continue;
                    }
                }

                if(input.equals(client.commands)) {
                    System.out.println(client.getPossibleCommands());
                    continue;
                }

                if(input.equals(client.read)){
                    client.readImpl();
                    continue;
                }

                if(input.equals(client.create)) {
                    client.createImpl();
                    System.out.println("Success");
                    continue;
                }

                if(input.equals(client.update)){
                    client.updateImpl();
                    continue;
                }

                if(input.equals(client.delete)) {
                    client.deleteImpl();
                    continue;
                }

                if(input.equals(client.addDancer)) {
                    client.addDancerImpl();
                    continue;
                }

                if(input.equals(client.addTrainer)){
                    client.addTrainerImpl();
                    continue;
                }

                if(input.equals(client.addSchool)) {
                    client.addSchoolsImpl();
                    continue;
                }

                if(input.equals(client.changeSchool)) {
                    client.changeSchoolImpl();
                    continue;
                }

                if(input.equals(client.getPotentialDancers)) {
                    client.getPotentialDancersImpl();
                    continue;
                }

                if(input.equals(client.findByName)){
                    client.findByNameImpl();
                    continue;
                }

                System.err.println("Error: unknown command");
                client.infoMsg();

            } catch(MyRestClientException e){
                System.err.println(e.toString());
            }

        }
    }

}
