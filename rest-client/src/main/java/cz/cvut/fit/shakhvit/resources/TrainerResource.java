package cz.cvut.fit.shakhvit.resources;

import cz.cvut.fit.shakhvit.dto.DancerDTO;
import cz.cvut.fit.shakhvit.dto.TrainerCreateDTO;
import cz.cvut.fit.shakhvit.dto.TrainerDTO;
import cz.cvut.fit.shakhvit.exception.RestTemplateErrorHandling;
import cz.cvut.fit.shakhvit.resources.TemplateResource;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

@Component
public class TrainerResource implements TemplateResource<TrainerCreateDTO, TrainerDTO> {
    private final RestTemplate restTemplate;

    public TrainerResource(RestTemplateBuilder restTemplateBuilder) {
        restTemplate = restTemplateBuilder.rootUri(ROOT_RESOURCE_URL).errorHandler(new RestTemplateErrorHandling()).build();
    }

    private static final String COLLECTION_PAGED_URL = "/?page={page}&size={size}";
    private static final String ROOT_RESOURCE_URL = "http://localhost:8080/api/v1/trainers";
    private static final String ONE_URI = "/{id}";

    public URI create(TrainerCreateDTO data) {
        return restTemplate.postForLocation("/", data);
    }

    public TrainerDTO update(int id, TrainerCreateDTO data) {
        HttpEntity<TrainerCreateDTO> entity = new HttpEntity<>(data);
        restTemplate.exchange(ONE_URI, HttpMethod.PUT, entity, TrainerCreateDTO.class, id);
        return readById(id);
    }

    public TrainerDTO readById(int id) {
       return restTemplate.getForObject(ONE_URI, TrainerDTO.class, id);
    }

    public PagedModel<TrainerDTO> readPage(int page, int size) {
        ResponseEntity<PagedModel<TrainerDTO>> result = restTemplate.exchange(
                COLLECTION_PAGED_URL, HttpMethod.GET, null,
                new ParameterizedTypeReference<PagedModel<TrainerDTO>>(){},
                page, size
        );
        return result.getBody();
    }

    public void deleteById(int id) {
        restTemplate.delete(ONE_URI, id);
    }

    public TrainerDTO addDancer(int id, int dancerId) {
        ResponseEntity<TrainerDTO> result = restTemplate.exchange(
                ONE_URI + "/addDancer/?dancerId={dancerId}",
                HttpMethod.PUT, null,
                new ParameterizedTypeReference<TrainerDTO>() {}, id, dancerId);
        return result.getBody();
    }

    public TrainerDTO addDanceSchool(int id, int schoolId) {
        ResponseEntity<TrainerDTO> result = restTemplate.exchange(
                ONE_URI + "/addSchool/?schoolId={schooldId}",
                HttpMethod.PUT, null,
                new ParameterizedTypeReference<TrainerDTO>() {}, id, schoolId
        );
        return result.getBody();
    }

    public PagedModel<DancerDTO> getPotentialDancers(int id, int page, int size) {
        ResponseEntity<PagedModel<DancerDTO>> result = restTemplate.exchange(
                ONE_URI + "/getPotentialDancers", HttpMethod.GET, null,
                new ParameterizedTypeReference<PagedModel<DancerDTO>>() {}, id, page, size);
        return result.getBody();
    }


}
