package cz.cvut.fit.shakhvit.entity;

import com.sun.istack.NotNull;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Entity
public class Trainer {

  @Id
  @GeneratedValue
  private int id;

  @NotNull
  private String firstName;

  @NotNull
  private String lastName;

  @ManyToMany
  @JoinTable(
          name="danceschool_trainer",
          joinColumns = @JoinColumn(name="trainer_id"),
          inverseJoinColumns = @JoinColumn(name="danceschool_id")
  )
  private List<DanceSchool> danceSchools;

  @ManyToMany
  @JoinTable(
          name="dancer_trainer",
          joinColumns = @JoinColumn(name="trainer_id"),
          inverseJoinColumns = @JoinColumn(name="dancer_id")
  )
  private List<Dancer> dancers;

  public Trainer() {}

  public Trainer( String firstName, String lastName, List<DanceSchool> danceSchools, List<Dancer> dancers ) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.danceSchools = danceSchools;
    this.dancers = dancers;
  }

  public int getId() {
    return id;
  }


  public String getFirstName() {
    return firstName;
  }

  public void setFirstName( String firstName ) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName( String lastName ) {
    this.lastName = lastName;
  }

  public List<DanceSchool> getDanceSchools() {
    return danceSchools;
  }

  public void setDanceSchools( List<DanceSchool> danceSchools ) {
    this.danceSchools = danceSchools;
  }

  public List<Dancer> getDancers() {
    return dancers;
  }

  public void setDancers( List<Dancer> dancers ) {
    this.dancers = dancers;
  }

  @Override
  public boolean equals( Object o ) {
    if ( this == o ) return true;
    if ( o == null || getClass() != o.getClass() ) return false;
    Trainer trainer = ( Trainer ) o;
    return id == trainer.id && firstName.equals( trainer.firstName ) && lastName.equals( trainer.lastName ) && Objects.equals( danceSchools, trainer.danceSchools ) && Objects.equals( dancers, trainer.dancers );
  }

  @Override
  public int hashCode() {
    return Objects.hash( id, firstName, lastName, danceSchools, dancers );
  }
}
