package cz.cvut.fit.shakhvit.assembler;

import cz.cvut.fit.shakhvit.contoller.DanceSchoolController;

import cz.cvut.fit.shakhvit.contoller.DancerController;
import cz.cvut.fit.shakhvit.dto.DanceSchoolDTO;
import cz.cvut.fit.shakhvit.dto.DancerDTO;
import cz.cvut.fit.shakhvit.entity.DanceSchool;
import cz.cvut.fit.shakhvit.entity.Dancer;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class DanceSchoolAssembler extends RepresentationModelAssemblerSupport<DanceSchool, DanceSchoolDTO> {
    public DanceSchoolAssembler() {
        super(DanceSchoolController.class, DanceSchoolDTO.class);
    }

    @Override
    public DanceSchoolDTO toModel(DanceSchool entity) {
        return new DanceSchoolDTO(
                entity.getId(),
                entity.getName(),
                entity.getNumberOfHalls(),
                entity.getDancers() == null ? null :
                        entity.getDancers().stream().map(i->i.getId()).collect(Collectors.toList()),
                entity.getTrainers() == null ? null :
                        entity.getTrainers().stream().map(i->i.getId()).collect(Collectors.toList())
        ).add(WebMvcLinkBuilder.linkTo(
                WebMvcLinkBuilder.methodOn(DanceSchoolController.class).readOne(entity.getId())
        ).withSelfRel());
    }
}
