package cz.cvut.fit.shakhvit.entity;

import com.sun.istack.NotNull;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Dancer{

  @Id
  @GeneratedValue
  private int id;

  @NotNull
  private String firstName;

  @NotNull
  private String lastName;

  @ManyToOne
  @JoinColumn(name="dancer_danceschools")
  private DanceSchool danceSchool;

  @ManyToMany
  @JoinTable(
          name = "dancer_trainer",
          joinColumns =  @JoinColumn(name="dancer_id"),
          inverseJoinColumns = @JoinColumn(name="trainer_id")
  )
  private List<Trainer> trainers;

  public Dancer() {}

  public Dancer( String firstName, String lastName, DanceSchool danceSchool, List<Trainer> trainers ) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.danceSchool = danceSchool;
    this.trainers = trainers;
  }

  public int getId() {
    return id;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName( String firstName ) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName( String lastName ) {
    this.lastName = lastName;
  }

  public DanceSchool getDanceSchool() {
    return danceSchool;
  }

  public void setDanceSchool( DanceSchool danceSchool ) {
    this.danceSchool = danceSchool;
  }

  public List<Trainer> getTrainers() {
    return trainers;
  }

  public void setTrainers( List<Trainer> trainers ) {
    this.trainers = trainers;
  }

  @Override
  public boolean equals( Object o ) {
    if ( this == o ) return true;
    if ( o == null || getClass() != o.getClass() ) return false;
    Dancer dancer = ( Dancer ) o;
    return id == dancer.id && firstName.equals( dancer.firstName ) && lastName.equals( dancer.lastName ) && Objects.equals( danceSchool, dancer.danceSchool ) && Objects.equals( trainers, dancer.trainers );
  }

  @Override
  public int hashCode() {
    return Objects.hash( id, firstName, lastName, danceSchool, trainers );
  }
}
