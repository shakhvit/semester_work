package cz.cvut.fit.shakhvit.assembler;

import cz.cvut.fit.shakhvit.contoller.DancerController;
import cz.cvut.fit.shakhvit.dto.DancerDTO;
import cz.cvut.fit.shakhvit.entity.Dancer;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class DancerAssembler extends RepresentationModelAssemblerSupport<Dancer, DancerDTO> {

    @Override
    public DancerDTO toModel(Dancer entity) {
        return new DancerDTO(
                entity.getId(),
                entity.getFirstName(),
                entity.getLastName(),
                entity.getDanceSchool() == null ? null :
                    entity.getDanceSchool().getId(),
                entity.getTrainers() == null ? null :
                    entity.getTrainers().stream().map(i->i.getId()).collect(Collectors.toList())
        ).add(WebMvcLinkBuilder.linkTo(
                WebMvcLinkBuilder.methodOn(DancerController.class).readOne(entity.getId())
        ).withSelfRel());
    }
    

    public DancerAssembler() {
        super(DancerController.class, DancerDTO.class);
    }
}
