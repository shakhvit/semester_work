package cz.cvut.fit.shakhvit.repository;

import cz.cvut.fit.shakhvit.entity.Trainer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TrainerRepository extends JpaRepository<Trainer, Integer> {}
