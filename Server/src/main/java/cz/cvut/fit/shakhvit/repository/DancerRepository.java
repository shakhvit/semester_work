package cz.cvut.fit.shakhvit.repository;

import cz.cvut.fit.shakhvit.entity.Dancer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DancerRepository extends JpaRepository<Dancer, Integer> {}
