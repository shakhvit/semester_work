package cz.cvut.fit.shakhvit.entity;


import com.sun.istack.NotNull;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(uniqueConstraints = {
        @UniqueConstraint( columnNames = { "NAME" })
})
public class DanceSchool {

  @Id
  @GeneratedValue
  private int id;

  @NotNull
  private String name; // name of dance school. Dance schools must have unique names.

  @NotNull
  private int numberOfHalls; // you cannot create dance school without dance halls. That's why it should be not null.

  @OneToMany
  @JoinColumn( name = "dancer_danceschools")
  private List<Dancer> dancers;

  @ManyToMany
  @JoinTable(
          name="danceschool_trainer",
          joinColumns = @JoinColumn(name="danceschool_id"),
          inverseJoinColumns = @JoinColumn(name="trainer_id")
  )
  private List<Trainer> trainers;

  public DanceSchool() {}

  public DanceSchool( String name, int numberOfHalls, List<Dancer> dancers, List<Trainer> trainers ) {
    this.name = name;
    this.numberOfHalls = numberOfHalls;
    this.dancers = dancers;
    this.trainers = trainers;
  }

  public int getId() {
    return id;
  }


  public String getName() {
    return name;
  }

  public void setName( String name ) {
    this.name = name;
  }

  public int getNumberOfHalls() {
    return numberOfHalls;
  }

  public void setNumberOfHalls( int numberOfHalls ) {
    this.numberOfHalls = numberOfHalls;
  }

  public List<Dancer> getDancers() {
    return dancers;
  }

  public void setDancers( List<Dancer> dancers ) {
    this.dancers = dancers;
  }

  public List<Trainer> getTrainers() {
    return trainers;
  }

  public void setTrainers( List<Trainer> trainers ) {
    this.trainers = trainers;
  }

  @Override
  public boolean equals( Object o ) {
    if ( this == o ) return true;
    if ( o == null || getClass() != o.getClass() ) return false;
    DanceSchool that = ( DanceSchool ) o;
    return id == that.id && numberOfHalls == that.numberOfHalls && name.equals( that.name ) && Objects.equals( dancers, that.dancers ) && Objects.equals( trainers, that.trainers );
  }

  @Override
  public int hashCode() {
    return Objects.hash( id, name, numberOfHalls, dancers, trainers );
  }
}
