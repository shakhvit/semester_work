package cz.cvut.fit.shakhvit.contoller;

import cz.cvut.fit.shakhvit.assembler.DancerAssembler;
import cz.cvut.fit.shakhvit.assembler.TrainerAssembler;
import cz.cvut.fit.shakhvit.dto.DancerDTO;
import cz.cvut.fit.shakhvit.dto.TrainerCreateDTO;
import cz.cvut.fit.shakhvit.dto.TrainerDTO;
import cz.cvut.fit.shakhvit.entity.Dancer;
import cz.cvut.fit.shakhvit.entity.Trainer;
import cz.cvut.fit.shakhvit.exceptions.ServerException;
import cz.cvut.fit.shakhvit.helpers.Helpers;
import cz.cvut.fit.shakhvit.service.TrainerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.PagedModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/v1/trainers")
public class TrainerController {
  private final TrainerService trainerService;
  private final PagedResourcesAssembler<Trainer> pagedResourcesAssembler;
  private final PagedResourcesAssembler<Dancer> dancerPagedResourcesAssembler;
  private final TrainerAssembler trainerAssembler;
  private final DancerAssembler dancerAssembler;

  @Autowired
  public TrainerController(TrainerService trainerService, PagedResourcesAssembler<Trainer> pagedResourcesAssembler, PagedResourcesAssembler<Dancer> dancerPagedResourcesAssembler, TrainerAssembler trainerAssembler, DancerAssembler dancerAssembler) {
    this.trainerService = trainerService;
    this.pagedResourcesAssembler = pagedResourcesAssembler;
    this.dancerPagedResourcesAssembler = dancerPagedResourcesAssembler;
    this.trainerAssembler = trainerAssembler;
    this.dancerAssembler = dancerAssembler;
  }

  @DeleteMapping("/{id}")
  public void delete(@PathVariable int id ) {
    try {
      trainerService.deleteById( id );
    } catch( ServerException e ) {
      throw  new ResponseStatusException( e.getStatus(), e.getMessage() );
    }
  }

  @GetMapping
  public PagedModel<TrainerDTO> readAll(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size) {
    List<Trainer> trainers = trainerService.findAll();
    return Helpers.getPage(page,size,trainerAssembler,trainers,pagedResourcesAssembler);
  }

  @GetMapping("/{id}")
  public TrainerDTO readOne( @PathVariable int id ) {
    try {
      return trainerAssembler.toModel(trainerService.findById(id));
    } catch( ServerException e ) {
      throw new ResponseStatusException( e.getStatus(), e.getMessage() );
    }
  }

  @PostMapping
  public ResponseEntity<TrainerDTO> create(@RequestBody TrainerCreateDTO trainerCreateDTO ) {
    try {
      TrainerDTO trainer = trainerService.create( trainerCreateDTO );
      trainer.add(WebMvcLinkBuilder.linkTo(
              WebMvcLinkBuilder.methodOn(TrainerController.class).readOne(trainer.getId())
      ).withSelfRel());
      return ResponseEntity.created(Link.of("http://localhost:8080/api/v1/trainers/"+trainer.getId()).toUri())
              .body(trainer);
    } catch( ServerException e ) {
      throw new ResponseStatusException( e.getStatus(), e.getMessage() );
    }
  }

  @PutMapping("/{id}")
  public TrainerDTO update( @PathVariable int id, @RequestBody TrainerCreateDTO trainerCreateDTO) {
    try {
      TrainerDTO toReturn = trainerService.update( id, trainerCreateDTO );
      return toReturn.add(WebMvcLinkBuilder.linkTo(
              WebMvcLinkBuilder.methodOn(TrainerController.class).readOne(toReturn.getId())
      ).withSelfRel());
    } catch( ServerException e ) {
      throw new ResponseStatusException( e.getStatus(), e.getMessage() );
    }
  }

  @PutMapping("/{id}/addSchool")
  public TrainerDTO addDanceSchool( @PathVariable int id, @RequestParam int schoolId ) {
    try {
      TrainerDTO trainer = trainerService.addDanceSchool( id, schoolId );
      return trainer.add(WebMvcLinkBuilder.linkTo(
              WebMvcLinkBuilder.methodOn(TrainerController.class).readOne(trainer.getId())
      ).withSelfRel());
    } catch( ServerException e ) {
      throw new ResponseStatusException( e.getStatus(), e.getMessage() );
    }
  }

  @PutMapping("/{id}/addDancer")
  public TrainerDTO addDancer( @PathVariable int id, @RequestParam int dancerId ) {
    try {
      TrainerDTO trainer = trainerService.addDancer( id, dancerId );
      return trainer.add(WebMvcLinkBuilder.linkTo(
              WebMvcLinkBuilder.methodOn(TrainerController.class).readOne(trainer.getId())
      ).withSelfRel());
    } catch( ServerException e ) {
      throw new ResponseStatusException( e.getStatus(), e.getMessage() );
    }
  }

  @GetMapping("/{id}/getPotentialDancers")
  public PagedModel<DancerDTO> getPotentialDancers(@PathVariable int id, @RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size ){
    try{
      List<Dancer> dancers = trainerService.getPotentialDancers(id);
      return Helpers.getPage(page,size,dancerAssembler,dancers,dancerPagedResourcesAssembler);
    } catch( ServerException e ) {
      throw new ResponseStatusException(e.getStatus(), e.getMessage());
    }
  }
}
