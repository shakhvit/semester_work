package cz.cvut.fit.shakhvit.repository;

import cz.cvut.fit.shakhvit.entity.DanceSchool;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface DanceSchoolRepository extends JpaRepository<DanceSchool, Integer> {
  Optional<DanceSchool> findByName(String name); // name of school is unique, so only one school should be returned

}
