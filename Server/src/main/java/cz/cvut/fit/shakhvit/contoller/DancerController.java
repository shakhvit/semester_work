package cz.cvut.fit.shakhvit.contoller;

import cz.cvut.fit.shakhvit.assembler.DancerAssembler;
import cz.cvut.fit.shakhvit.dto.DancerCreateDTO;
import cz.cvut.fit.shakhvit.dto.DancerDTO;
import cz.cvut.fit.shakhvit.entity.Dancer;
import cz.cvut.fit.shakhvit.exceptions.ServerException;
import cz.cvut.fit.shakhvit.helpers.Helpers;
import cz.cvut.fit.shakhvit.service.DancerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.PagedModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;


import java.util.List;

@RestController
@RequestMapping("/api/v1/dancers")
public class DancerController {
  private final DancerService dancerService;
  private final PagedResourcesAssembler<Dancer> pagedResourcesAssembler;
  private final DancerAssembler dancerAssembler;


  @Autowired
  public DancerController(DancerService dancerService, PagedResourcesAssembler<Dancer> pagedResourcesAssembler, DancerAssembler dancerAssembler) {
    this.dancerService = dancerService;
    this.pagedResourcesAssembler = pagedResourcesAssembler;
    this.dancerAssembler = dancerAssembler;
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public ResponseEntity<DancerDTO> create(@RequestBody DancerCreateDTO dancerDTO ) {
    try {
      DancerDTO dancer = dancerService.create( dancerDTO );
      dancer.add(WebMvcLinkBuilder.linkTo(
              WebMvcLinkBuilder.methodOn(DancerController.class).readOne(dancer.getId())
      ).withSelfRel());
      return ResponseEntity
              .created(Link.of("http://localhost:8080/api/v1/dancers/"+dancer.getId()).toUri())
              .body(dancer);
    }
    catch( ServerException e ) {
      throw new ResponseStatusException( e.getStatus(), e.getMessage() );
    }
  }

  @GetMapping("/{id}")
  public DancerDTO readOne( @PathVariable int id ) {
    try {
      return dancerAssembler.toModel(dancerService.findById(id));
    } catch( ServerException e ) {
      throw new ResponseStatusException( e.getStatus(), e.getMessage() );
    }
  }


  @GetMapping()
  public PagedModel<DancerDTO> readAll(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size) {
    List<Dancer> dancers = dancerService.findAll();
    return Helpers.getPage(page,size,dancerAssembler,dancers,pagedResourcesAssembler);
  }

  @PutMapping("/{id}")
  public ResponseEntity<DancerDTO>  update( @PathVariable  int id, @RequestBody  DancerCreateDTO dancerCreateDTO ) {
    try {
      DancerDTO dancer = dancerService.update( id, dancerCreateDTO );
      return ResponseEntity.accepted().body(dancer.add(
              WebMvcLinkBuilder.linkTo(
                      WebMvcLinkBuilder.methodOn(DancerController.class).readOne(dancer.getId())
              ).withSelfRel()
      ));
    }
    catch( ServerException e ) {
      throw new ResponseStatusException( e.getStatus(), e.getMessage() );
    }
  }

  @DeleteMapping("/{id}")
  @ResponseStatus(HttpStatus.OK)
  public void delete( @PathVariable int id ) {
    try {
      dancerService.deleteById( id );
    } catch( ServerException e ) {
      throw new ResponseStatusException( e.getStatus(), e.getMessage() );
    }
  }

  @PutMapping("/{id}/changeSchool")
  public DancerDTO changeDanceSchool( @PathVariable int id, @RequestParam int schoolId ) {
    try {
      DancerDTO toReturn = dancerService.changeDanceSchool(id, schoolId );
      return toReturn.add(WebMvcLinkBuilder.linkTo(
              WebMvcLinkBuilder.methodOn(DancerController.class).readOne(toReturn.getId())
      ).withSelfRel());
    } catch( ServerException e ) {
      throw new ResponseStatusException( e.getStatus(), e.getMessage() );
    }
  }

  @PutMapping("/{id}/addTrainer")
  public DancerDTO addTrainer( @PathVariable int id, @RequestParam int trainerId ) {
    try {
      DancerDTO toReturn = dancerService.addTrainer( id, trainerId );
      return toReturn.add(
              WebMvcLinkBuilder.linkTo(
                      WebMvcLinkBuilder.methodOn(DancerController.class).readOne(toReturn.getId())
              ).withSelfRel()
      );
    } catch( ServerException e ) {
      throw new ResponseStatusException( e.getStatus(), e.getMessage() );
    }
  }

}
