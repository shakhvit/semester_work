package cz.cvut.fit.shakhvit.service;

import cz.cvut.fit.shakhvit.dto.DanceSchoolCreateDTO;
import cz.cvut.fit.shakhvit.dto.DanceSchoolDTO;
import cz.cvut.fit.shakhvit.entity.DanceSchool;
import cz.cvut.fit.shakhvit.entity.Dancer;
import cz.cvut.fit.shakhvit.entity.Trainer;
import cz.cvut.fit.shakhvit.exceptions.ServerException;
import cz.cvut.fit.shakhvit.repository.DanceSchoolRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DanceSchoolService {

  private DancerService dancerService;
  private TrainerService trainerService;
  private DanceSchoolRepository danceSchoolRepository;

  @Autowired
  public void setDancerService( DancerService dancerService ) {
    this.dancerService = dancerService;
  }

  @Autowired
  public void setTrainerService( TrainerService trainerService ) {
    this.trainerService = trainerService;
  }


  @Autowired
  public void setDanceSchoolRepository( DanceSchoolRepository danceSchoolRepository ) {
    this.danceSchoolRepository = danceSchoolRepository;
  }

  public DanceSchool findById( int id ) throws ServerException {
    return danceSchoolRepository.findById( id ).orElseThrow(
            () -> new ServerException( HttpStatus.NOT_FOUND, "Dance school was not found" )
    );
  }

  public List<DanceSchool> findByIds( List<Integer> ids ) throws ServerException {
    List<DanceSchool> danceSchools = danceSchoolRepository.findAllById( ids );
    if( danceSchools.size() != ids.size() )
      throw new ServerException( HttpStatus.NOT_FOUND, "Dance school was not found" );
    return danceSchools;
  }

  public List<DanceSchool> findAll() {
    return danceSchoolRepository.findAll();
  }

  public void deleteById( int id )  throws  ServerException{
    findById( id );
    danceSchoolRepository.deleteById( id );
  }


  public DanceSchoolDTO findByIdDTO(int id) throws ServerException {
    return toDTO(findById( id ));
  }

  public List<DanceSchoolDTO> findAllDTO() {
    return findAll()
            .stream()
            .map(this::toDTO)
            .collect( Collectors.toList() );
  }

  public DanceSchool findByName( String name ) throws ServerException{
    return danceSchoolRepository.findByName( name ).orElseThrow(
            () -> new ServerException( HttpStatus.NOT_FOUND, "Dance school with this name doesn't exist")
    );
  }

  public boolean existsName( String name ) {
    Optional<DanceSchool> danceSchool = danceSchoolRepository.findByName( name );
    return !danceSchool.isEmpty();
  }

  public DanceSchoolDTO create( DanceSchoolCreateDTO danceSchoolCreateDTO ) throws ServerException{
    if( existsName( danceSchoolCreateDTO.getName() ) )
      throw new ServerException( HttpStatus.CONFLICT, "Dance school with the same name already exists" );
    DanceSchool danceSchool = new DanceSchool(
            danceSchoolCreateDTO.getName(),
            danceSchoolCreateDTO.getNumberOfHalls(),
            getDancersOrNull( danceSchoolCreateDTO.getDancersIds() ),
            getTrainersOrNull( danceSchoolCreateDTO.getTrainersIds() )
    );
    danceSchoolRepository.save( danceSchool );
    return toDTO(  danceSchool );
  }

  @Transactional
  public DanceSchoolDTO update( int id, DanceSchoolCreateDTO danceSchoolCreateDTO ) throws  ServerException{
    Optional<DanceSchool> optionalDanceSchool = danceSchoolRepository.findById(id);
    DanceSchool danceSchool = optionalDanceSchool.get();
    danceSchool.setName( danceSchoolCreateDTO.getName() );
    danceSchool.setNumberOfHalls( danceSchoolCreateDTO.getNumberOfHalls() );
    danceSchool.setDancers( getDancersOrNull( danceSchoolCreateDTO.getDancersIds() ) );
    danceSchool.setTrainers( getTrainersOrNull( danceSchoolCreateDTO.getTrainersIds() ) );
    return toDTO( danceSchool );
  }


  @Transactional
  public DanceSchoolDTO addDancer( int id, int dancerId ) throws ServerException {
    DanceSchool danceSchool = findById( id );
    Dancer dancer = dancerService.findById( dancerId );
    List<Dancer> dancers = danceSchool.getDancers();
    if( dancers == null ){
      dancers = Arrays.asList( dancer );
      danceSchool.setDancers( dancers );
    }
    else if( !dancers.contains( dancer )) {
      dancers.add( dancer );
      danceSchool.setDancers( dancers );
    }
    return toDTO( danceSchool );
  }

  @Transactional
  public DanceSchoolDTO addTrainer( int id, int trainerId ) throws ServerException {
    DanceSchool danceSchool = findById( id );
    Trainer trainer = trainerService.findById( trainerId );
    List<Trainer> trainers = danceSchool.getTrainers();
    if( trainers == null ) {
      trainers = Arrays.asList( trainer );
      danceSchool.setTrainers( trainers );
    }
    else if( !trainers.contains( trainer )) {
      trainers.add( trainer );
      danceSchool.setTrainers( trainers );
    }
    return toDTO( danceSchool );
  }

  private DanceSchoolDTO toDTO( DanceSchool danceSchool ) {
    return new DanceSchoolDTO(
            danceSchool.getId(),
            danceSchool.getName(),
            danceSchool.getNumberOfHalls(),
            danceSchool.getDancers() == null ? null :
              danceSchool.getDancers().stream().map( Dancer::getId ).collect( Collectors.toList()),
            danceSchool.getTrainers() == null ? null :
              danceSchool.getTrainers().stream().map( Trainer::getId ).collect( Collectors.toList()) );
  }

  private Optional<DanceSchoolDTO> toDTO( Optional<DanceSchool> danceSchool ) {
    if( danceSchool.isEmpty() )
      return Optional.empty();
    return Optional.of(toDTO(danceSchool.get()));
  }

  private List<Trainer> getTrainersOrNull( List<Integer> ids ) throws ServerException {
    if( ids == null ) return null;
    return trainerService.findByIds( ids );
  }

  private List<Dancer> getDancersOrNull( List<Integer> ids ) throws ServerException {
    if( ids == null ) return null;
    return dancerService.findByIds( ids );
  }
}
