package cz.cvut.fit.shakhvit.helpers;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedModel;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;

import java.util.List;

public class Helpers {

    public static <Type, TypeDTO extends RepresentationModel<TypeDTO>, Assembler extends RepresentationModelAssemblerSupport<Type, TypeDTO>>
    PagedModel<TypeDTO> getPage(int page, int size, Assembler assembler, List<Type> list, PagedResourcesAssembler<Type> pagedResourcesAssembler) {
        Pageable pageable = PageRequest.of(page,size);
        int start = (int)pageable.getOffset();
        int end = (start + pageable.getPageSize() > list.size()) ? list.size() : start + (int)pageable.getPageSize();
        Page<Type> pageL = new PageImpl<>(list.subList(start,end), pageable, list.size());
        return pagedResourcesAssembler.toModel(pageL, assembler);
    }
}
