package cz.cvut.fit.shakhvit.service;

import cz.cvut.fit.shakhvit.entity.DanceSchool;
import cz.cvut.fit.shakhvit.entity.Dancer;
import cz.cvut.fit.shakhvit.entity.Trainer;
import cz.cvut.fit.shakhvit.exceptions.ServerException;
import cz.cvut.fit.shakhvit.repository.TrainerRepository;
import cz.cvut.fit.shakhvit.dto.DancerDTO;
import cz.cvut.fit.shakhvit.dto.TrainerCreateDTO;
import cz.cvut.fit.shakhvit.dto.TrainerDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class TrainerService{
  private DanceSchoolService danceSchoolService;
  private DancerService  dancerService;
  private TrainerRepository trainerRepository;

  @Autowired
  public void setDanceSchoolService( DanceSchoolService danceSchoolService ) {
    this.danceSchoolService = danceSchoolService;
  }

  @Autowired
  public void setDancerService( DancerService dancerService ) {
    this.dancerService = dancerService;
  }


  @Autowired
  public void setTrainerRepository( TrainerRepository trainerRepository ) {
    this.trainerRepository = trainerRepository;
  }

  public Trainer findById( int id ) throws ServerException {
    return trainerRepository.findById( id ).orElseThrow(
            () -> new ServerException( HttpStatus.NOT_FOUND, "Trainer not found")
    );
  }

  public List<Trainer> findByIds( List<Integer> ids ) throws ServerException {
    List<Trainer> trainers = trainerRepository.findAllById( ids );
    if( trainers.size() != ids.size() )
      throw new ServerException( HttpStatus.NOT_FOUND, "Trainer not found" );
    return trainers;
  }

  public List<Trainer> findAll() {
    return trainerRepository.findAll();
  }

  public void deleteById( int id ) throws ServerException{
    findById( id );
    trainerRepository.deleteById( id );
  }


  public TrainerDTO findByIdDTO(int id ) throws ServerException {
    return toDTO( findById( id ) );
  }

  public List<TrainerDTO> findByIdsDTO( List<Integer> ids ) throws ServerException {
    return findByIds( ids ).stream().
            map( this::toDTO ).collect( Collectors.toList());
  }

  public List<TrainerDTO> findAllDTO() {
    return findAll().stream().
            map( this::toDTO ).collect( Collectors.toList());
  }

  public TrainerDTO create( TrainerCreateDTO trainerCreateDTO ) throws  ServerException{
    Trainer trainer = new Trainer(trainerCreateDTO.getFirstName(),
            trainerCreateDTO.getLastName(),
            getDanceSchoolsOrNull( trainerCreateDTO.getDanceSchoolsIds()),
            getDancersOrNull( trainerCreateDTO.getDancersIds() )
    );
    trainerRepository.save( trainer );
    return toDTO( trainer );
  }

  @Transactional
  public TrainerDTO update( int id, TrainerCreateDTO trainerCreateDTO ) throws ServerException {
    Trainer trainer = findById( id  );
    trainer.setFirstName( trainerCreateDTO.getFirstName() );
    trainer.setLastName( trainerCreateDTO.getLastName() );
    trainer.setDanceSchools( getDanceSchoolsOrNull( trainerCreateDTO.getDanceSchoolsIds() ) );
    trainer.setDancers( getDancersOrNull( trainerCreateDTO.getDancersIds() ) );
    return toDTO( trainer );
  }

  @Transactional
  public TrainerDTO addDanceSchool( int id, int danceSchoolId ) throws ServerException {
    Trainer trainer = findById( id );
    DanceSchool danceSchool = danceSchoolService.findById( danceSchoolId );
    List<DanceSchool> danceSchools = trainer.getDanceSchools();
    if( danceSchools == null ) {
      danceSchools = Arrays.asList( danceSchool );
      trainer.setDanceSchools( danceSchools );
    }
    else if( !danceSchools.contains( danceSchool ) ) {
      danceSchools.add( danceSchool );
      trainer.setDanceSchools( danceSchools );
    }
    return toDTO( trainer );
  }

  @Transactional
  public TrainerDTO addDancer( int id, int dancerId ) throws ServerException {
    Trainer trainer = findById( id );
    Dancer dancer = dancerService.findById( dancerId );
    List<Dancer> dancers = trainer.getDancers();
    if( dancers == null ) {
      dancers = Arrays.asList( dancer );
      trainer.setDancers( dancers );
    }
    else if( !dancers.contains( dancer )){
      dancers.add( dancer );
      trainer.setDancers( dancers );
    }
    return toDTO( trainer );
  }

  public List<Dancer> getPotentialDancers(int id) throws ServerException {
    Trainer trainer = findById(id);
    ArrayList<Dancer> dancers = new ArrayList<>();
    HashSet<Integer> dancersIds = new HashSet<Integer>();
    if(trainer.getDanceSchools()!=null){
      trainer.getDanceSchools().stream().forEach((school) -> {
        if(school.getDancers()!=null){
          school.getDancers().stream().forEach((dancer) -> {
            if(!dancersIds.contains(dancer.getId())){
              dancersIds.add(dancer.getId());
              dancers.add(dancer);
            }
          });
        }
      });
    }
    return dancers;
  }

  private TrainerDTO toDTO( Trainer trainer ) {
    return new TrainerDTO(
            trainer.getId(),
            trainer.getFirstName(),
            trainer.getLastName(),
            trainer.getDanceSchools() == null ? null :
              trainer.getDanceSchools().stream().
                      map( DanceSchool::getId ).collect( Collectors.toList()),
            trainer.getDancers() == null ? null :
              trainer.getDancers().stream().
                      map( Dancer::getId ).collect( Collectors.toList()) );
  }

  private Optional<TrainerDTO> toDTO( Optional<Trainer> optionalTrainer ) {
    if( optionalTrainer.isEmpty() )
      return Optional.empty();
    return Optional.of( toDTO( optionalTrainer.get() ));
  }

  private List<DanceSchool> getDanceSchoolsOrNull( List<Integer> ids ) throws ServerException{
    if( ids == null ) return null;

    List<DanceSchool> danceSchools = danceSchoolService.findByIds( ids );
    if( danceSchools.size() != ids.size() )
      throw new ServerException( HttpStatus.NOT_FOUND, "Dance school was not found" );
    return danceSchools;
  }

  private List<Dancer> getDancersOrNull( List<Integer> ids ) throws ServerException {
    if( ids == null ) return null;
    List<Dancer> dancers = dancerService.findByIds( ids );
    if( dancers.size() != ids.size() )
      throw new ServerException( HttpStatus.NOT_FOUND, "Dancer was not found" );
    return dancers;
  }
}
