package cz.cvut.fit.shakhvit.contoller;

import cz.cvut.fit.shakhvit.assembler.DanceSchoolAssembler;
import cz.cvut.fit.shakhvit.dto.DanceSchoolCreateDTO;
import cz.cvut.fit.shakhvit.dto.DanceSchoolDTO;
import cz.cvut.fit.shakhvit.entity.DanceSchool;
import cz.cvut.fit.shakhvit.exceptions.ServerException;
import cz.cvut.fit.shakhvit.helpers.Helpers;
import cz.cvut.fit.shakhvit.service.DanceSchoolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.PagedModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/v1/schools")
public class DanceSchoolController {

  private DanceSchoolService danceSchoolService;
  private final PagedResourcesAssembler<DanceSchool> pagedResourcesAssembler;
  private final DanceSchoolAssembler danceSchoolAssembler;

  public DanceSchoolController(PagedResourcesAssembler<DanceSchool> pagedResourcesAssembler, DanceSchoolAssembler danceSchoolAssembler) {this.pagedResourcesAssembler = pagedResourcesAssembler;
    this.danceSchoolAssembler = danceSchoolAssembler;
  }

  @Autowired
  public void setDanceSchoolService( DanceSchoolService danceSchoolService ) {
    this.danceSchoolService = danceSchoolService;
  }

  @GetMapping()
  public PagedModel<DanceSchoolDTO> readAll(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size) {
    List<DanceSchool> schools = danceSchoolService.findAll();
    return Helpers.getPage(page,size,danceSchoolAssembler,schools,pagedResourcesAssembler);
  }

  @GetMapping("/{id}")
  public DanceSchoolDTO readOne( @PathVariable int id ) {
    try {
      return danceSchoolAssembler.toModel(danceSchoolService.findById(id));
    } catch( ServerException e ) {
      throw new ResponseStatusException( e.getStatus(), e.getMessage() );
    }
  }

  @GetMapping("/find")
  public DanceSchoolDTO findByName( @RequestParam String name ) {
    try{
      return danceSchoolAssembler.toModel(danceSchoolService.findByName( name ));
    } catch( ServerException e ) {
      throw new ResponseStatusException( e.getStatus(), e.getMessage() );
    }
  }

  @PostMapping()
  public ResponseEntity<DanceSchoolDTO> create(@RequestBody DanceSchoolCreateDTO danceSchoolCreateDTO ) {
    try {
      DanceSchoolDTO school = danceSchoolService.create( danceSchoolCreateDTO );
      school.add(WebMvcLinkBuilder.linkTo(
              WebMvcLinkBuilder.methodOn(DanceSchoolController.class).readOne(school.getId())
      ).withSelfRel());
      return ResponseEntity
              .created(Link.of("http://localhost:8080/api/v1/schools"+school.getId()).toUri())
              .body(school);
    } catch( ServerException e ) {
      throw new ResponseStatusException( e.getStatus(), e.getMessage() );
    }
  }

  @PutMapping("/{id}")
  public ResponseEntity<DanceSchoolDTO> update( @PathVariable int id, @RequestBody DanceSchoolCreateDTO danceSchoolCreateDTO ) {
    try {
      DanceSchoolDTO school = danceSchoolService.update( id, danceSchoolCreateDTO );
      school.add(WebMvcLinkBuilder.linkTo(
              WebMvcLinkBuilder.methodOn(DanceSchoolController.class).readOne(school.getId())
      ).withSelfRel());
      return ResponseEntity.accepted().body(school);
    } catch( ServerException e ) {
      throw new ResponseStatusException( e.getStatus(), e.getMessage() );
    }
  }

  @DeleteMapping("/{id}")
  public void delete( @PathVariable int id ) {
    try {
      danceSchoolService.deleteById( id );
    } catch( ServerException e ) {
      throw new ResponseStatusException( e.getStatus(), e.getMessage() );
    }
  }

  @PutMapping("/{id}/addTrainer")
  public DanceSchoolDTO addTrainer( @PathVariable int id, @RequestParam int trainerId ) {
    try {
      DanceSchoolDTO toReturn = danceSchoolService.addTrainer( id, trainerId );
      return toReturn.add(WebMvcLinkBuilder.linkTo(
              WebMvcLinkBuilder.methodOn(DanceSchoolController.class).readOne(toReturn.getId())
      ).withSelfRel());
    } catch( ServerException e ) {
      throw new ResponseStatusException( e.getStatus(), e.getMessage() );
    }
  }

  @PutMapping("/{id}/addDancer")
  public DanceSchoolDTO addDancer(@PathVariable int id, @RequestParam int dancerId ) {
    try {
      DanceSchoolDTO toReturn = danceSchoolService.addDancer( id, dancerId );
      return toReturn.add(WebMvcLinkBuilder.linkTo(
              WebMvcLinkBuilder.methodOn(DanceSchoolController.class).readOne(toReturn.getId())
      ).withSelfRel());
    } catch( ServerException e ) {
      throw new ResponseStatusException( e.getStatus(), e.getMessage() );
    }
  }

}
