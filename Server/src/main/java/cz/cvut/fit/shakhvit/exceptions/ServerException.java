package cz.cvut.fit.shakhvit.exceptions;

import org.springframework.http.HttpStatus;

public class ServerException  extends  Exception {
  private final HttpStatus status;
  private final String message;

  public ServerException(HttpStatus status, String message ) {
    this.status = status;
    this.message = message;
  }

  public HttpStatus getStatus() {
    return status;
  }

  public String getMessage() {
    return message;
  }

}
