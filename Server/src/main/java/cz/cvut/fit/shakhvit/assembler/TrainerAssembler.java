package cz.cvut.fit.shakhvit.assembler;

import cz.cvut.fit.shakhvit.contoller.DancerController;
import cz.cvut.fit.shakhvit.contoller.TrainerController;
import cz.cvut.fit.shakhvit.dto.TrainerDTO;
import cz.cvut.fit.shakhvit.entity.DanceSchool;
import cz.cvut.fit.shakhvit.entity.Trainer;
import cz.cvut.fit.shakhvit.service.TrainerService;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class TrainerAssembler extends RepresentationModelAssemblerSupport<Trainer, TrainerDTO> {

    public TrainerAssembler() { super(TrainerController.class, TrainerDTO.class); }

    @Override
    public TrainerDTO toModel(Trainer entity) {
        return new TrainerDTO(
                entity.getId(),
                entity.getFirstName(),
                entity.getLastName(),
                entity.getDanceSchools() == null ? null :
                    entity.getDanceSchools().stream().map(i->i.getId()).collect(Collectors.toList()),
                entity.getDancers() == null ? null :
                    entity.getDancers().stream().map(i->i.getId()).collect(Collectors.toList()))
                .add(WebMvcLinkBuilder.linkTo(
                        WebMvcLinkBuilder.methodOn(TrainerController.class).readOne(entity.getId())
                ).withSelfRel());
    }
}
