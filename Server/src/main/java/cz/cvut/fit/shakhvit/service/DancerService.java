package cz.cvut.fit.shakhvit.service;

import cz.cvut.fit.shakhvit.dto.DancerCreateDTO;
import cz.cvut.fit.shakhvit.dto.DancerDTO;
import cz.cvut.fit.shakhvit.entity.DanceSchool;
import cz.cvut.fit.shakhvit.entity.Dancer;
import cz.cvut.fit.shakhvit.entity.Trainer;
import cz.cvut.fit.shakhvit.exceptions.ServerException;
import cz.cvut.fit.shakhvit.repository.DancerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DancerService {
  private DanceSchoolService danceSchoolService;
  private TrainerService trainerService;
  private DancerRepository dancerRepository;

  public  DancerService() {}

  @Autowired
  public void setDanceSchoolService( DanceSchoolService danceSchoolService ) {
    this.danceSchoolService = danceSchoolService;
  }

  @Autowired
  public void setTrainerService( TrainerService trainerService ) {
    this.trainerService = trainerService;
  }

  @Autowired
  public void setDancerRepository( DancerRepository dancerRepository ) {
    this.dancerRepository = dancerRepository;
  }


  public Dancer findById( int id ) throws ServerException {
    Optional<Dancer> dancer = dancerRepository.findById( id );
    if( dancer.isEmpty() )
      throw new ServerException( HttpStatus.NOT_FOUND, "Dancer with this ID was not found" );
    return dancer.get();
  }

  public List<Dancer> findByIds( List<Integer> ids ) throws ServerException {
    List<Dancer> dancers = dancerRepository.findAllById( ids );
    if( dancers.size() != ids.size() )
      throw new ServerException(HttpStatus.NOT_FOUND, "Dancer was not found");
    return dancers;
  }

  public List<Dancer> findAll() {
    return dancerRepository.findAll();
  }

  public void deleteById( int id ) throws ServerException {
    findById( id ); // it will throw an exception, if there is no such id
    dancerRepository.deleteById( id );
  }


  public DancerDTO findByIdDTO( int id ) throws ServerException {
    return toDTO( findById( id ));
  }

  public List<DancerDTO> findAllDTO() {
    return findAll()
            .stream()
            .map( this::toDTO )
            .collect( Collectors.toList());
  }

  public DancerDTO create( DancerCreateDTO dancerCreateDTO ) throws ServerException{
    DanceSchool danceSchool = dancerCreateDTO.getDanceSchoolId() == null ?
            null                                                         :
            getDanceSchoolOrNull( dancerCreateDTO.getDanceSchoolId() );

    Dancer dancer = new Dancer(
            dancerCreateDTO.getFirstName(),
            dancerCreateDTO.getLastName(),
            danceSchool,
            getTrainersOrNull( dancerCreateDTO.getTrainersIds() )
    );
    dancerRepository.save( dancer );
    return toDTO( dancer );
  }

  @Transactional
  public DancerDTO update( int id, DancerCreateDTO dancerCreateDTO ) throws ServerException{
    Dancer dancer = findById( id );
    DanceSchool danceSchool= dancerCreateDTO.getDanceSchoolId() == null ? null :
            getDanceSchoolOrNull( dancerCreateDTO.getDanceSchoolId() );
    dancer.setFirstName( dancerCreateDTO.getFirstName() );
    dancer.setLastName( dancerCreateDTO.getLastName() );
    dancer.setDanceSchool( danceSchool );
    dancer.setTrainers( getTrainersOrNull( dancerCreateDTO.getTrainersIds() ) );
    return toDTO(dancer);
  }

  @Transactional
  public DancerDTO changeDanceSchool( int id, int danceSchoolId ) throws ServerException {
    Dancer dancer = findById( id );
    DanceSchool danceSchool = danceSchoolService.findById( danceSchoolId );
    dancer.setDanceSchool( danceSchool );
    return toDTO( dancer );
  }

  @Transactional
  public DancerDTO addTrainer( int id, int trainerId ) throws ServerException {
    Dancer dancer = findById( id );
    Trainer trainer = trainerService.findById( trainerId );
    List<Trainer> trainers = dancer.getTrainers();
    if( trainers == null ) {
      trainers = Arrays.asList( trainer );
      dancer.setTrainers( trainers );
    }
    else if( !trainers.contains( trainer )) {
      trainers.add( trainer );
      dancer.setTrainers( trainers );
    }
    return toDTO( dancer );
  }


  public DancerDTO toDTO(Dancer dancer ) {
    return new DancerDTO(
            dancer.getId(),
            dancer.getFirstName(),
            dancer.getLastName(),
            dancer.getDanceSchool() == null ? null :
                      dancer.getDanceSchool().getId(),
            dancer.getTrainers() == null ? null :
            dancer.getTrainers().stream().map( Trainer::getId ).collect( Collectors.toList())
    );
  }

  private Optional<DancerDTO> toDTO( Optional<Dancer> optionalDancer ) throws ServerException{
    if( optionalDancer.isEmpty() )
      return Optional.empty();
    return Optional.of( toDTO(optionalDancer.get() ));
  }

  private DanceSchool getDanceSchoolOrNull( int id ) throws  ServerException {
    return danceSchoolService.findById( id );
  }

  private List<Trainer> getTrainersOrNull( List<Integer> ids ) throws ServerException {
    if( ids == null ) return null;
    return trainerService.findByIds( ids  );
  }
}

