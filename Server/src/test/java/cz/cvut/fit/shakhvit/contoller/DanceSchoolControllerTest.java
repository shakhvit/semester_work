package cz.cvut.fit.shakhvit.contoller;

import cz.cvut.fit.shakhvit.dto.DanceSchoolCreateDTO;
import cz.cvut.fit.shakhvit.dto.DanceSchoolDTO;
import cz.cvut.fit.shakhvit.dto.DancerDTO;
import cz.cvut.fit.shakhvit.entity.DanceSchool;
import cz.cvut.fit.shakhvit.service.DanceSchoolService;
import jdk.jshell.DeclarationSnippet;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;

@SpringBootTest
@AutoConfigureMockMvc
public class DanceSchoolControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DanceSchoolService danceSchoolService;

    @Test
    void create() throws Exception {
        DanceSchoolDTO created = new DanceSchoolDTO(1,"Best dance school",3,null,null);
        DanceSchoolCreateDTO toCreate = new DanceSchoolCreateDTO(
                "Best dance school", 3, null, null
        );

        BDDMockito.given(danceSchoolService.create(toCreate)).willReturn(created);

        mockMvc.perform(
                MockMvcRequestBuilders.post("/api/v1/schools")
                .contentType("application/json")
                .content("{ \"name\": \"Best dance school\", \"numberOfHalls\": \"3\" }")
        ).andExpect(MockMvcResultMatchers.status().isCreated());

        Mockito.verify(danceSchoolService,Mockito.atLeastOnce()).create(toCreate);
    }

    @Test
    void readOne() throws Exception {
        DanceSchool toReturn = new DanceSchool( "Best dance school", 3, null, null);
        ReflectionTestUtils.setField(toReturn, "id", 1);
        BDDMockito.given(danceSchoolService.findById(1)).willReturn(toReturn);

        mockMvc.perform(
                MockMvcRequestBuilders.get("/api/v1/schools/{id}", toReturn.getId())
        ).andExpect(MockMvcResultMatchers.status().isOk())
         .andExpect(MockMvcResultMatchers.jsonPath("$.name", CoreMatchers.is(toReturn.getName())))
         .andExpect(MockMvcResultMatchers.jsonPath("$.numberOfHalls", CoreMatchers.is(toReturn.getNumberOfHalls())));

        Mockito.verify(danceSchoolService, Mockito.atLeastOnce()).findById(toReturn.getId());
    }

    @Test
    void readAll() throws Exception {
        DanceSchool toReturn1 = new DanceSchool("Best dance school1",3, null,null);
        DanceSchool toReturn2 = new DanceSchool("Best dance school2",4, null,null);
        ReflectionTestUtils.setField(toReturn1, "id", 1);
        ReflectionTestUtils.setField(toReturn2, "id", 2);

        DanceSchoolDTO toReturn1DTO = new DanceSchoolDTO(toReturn1.getId(), toReturn1.getName(), toReturn1.getNumberOfHalls(), null, null);
        DanceSchoolDTO toReturn2DTO = new DanceSchoolDTO(toReturn2.getId(), toReturn2.getName(), toReturn2.getNumberOfHalls(), null, null);

        BDDMockito.given(danceSchoolService.findAll()).willReturn(Arrays.asList(toReturn1, toReturn2));

        mockMvc.perform(
                MockMvcRequestBuilders.get("/api/v1/schools")
        ).andExpect(MockMvcResultMatchers.status().isOk())
         .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.danceSchoolDTOList.[0].id", CoreMatchers.is(toReturn1DTO.getId())))
         .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.danceSchoolDTOList.[0].name", CoreMatchers.is(toReturn1DTO.getName())))
         .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.danceSchoolDTOList.[0].numberOfHalls", CoreMatchers.is(toReturn1DTO.getNumberOfHalls())))
         .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.danceSchoolDTOList.[0]._links.self.href", CoreMatchers.endsWith("/api/v1/schools/1")))
         .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.danceSchoolDTOList.[1].id", CoreMatchers.is(toReturn2DTO.getId())))
         .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.danceSchoolDTOList.[1].name", CoreMatchers.is(toReturn2DTO.getName())))
         .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.danceSchoolDTOList.[1].numberOfHalls", CoreMatchers.is(toReturn2DTO.getNumberOfHalls())))
         .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.danceSchoolDTOList.[1]._links.self.href", CoreMatchers.endsWith("/api/v1/schools/2")));

        Mockito.verify(danceSchoolService,Mockito.atLeastOnce()).findAll();
    }

    @Test
    void update() throws Exception {
        DanceSchoolDTO toReturn = new DanceSchoolDTO(1, "Best dance school", 3,null, null);
        DanceSchoolCreateDTO toUpdate = new DanceSchoolCreateDTO("Best dance school", 3, null, null);

        BDDMockito.given(danceSchoolService.update(1,toUpdate)).willReturn(toReturn);

        mockMvc.perform(
                MockMvcRequestBuilders.put("/api/v1/schools/{id}", toReturn.getId())
                .contentType("application/json")
                .content("{ \"name\": \"Best dance school\", \"numberOfHalls\": \"3\" } ")
        ).andExpect(MockMvcResultMatchers.status().isAccepted());

        Mockito.verify(danceSchoolService,Mockito.atLeastOnce()).update(toReturn.getId(), toUpdate);

    }

    @Test
    void delete() throws Exception {

        mockMvc.perform(
                MockMvcRequestBuilders.delete("/api/v1/schools/{id}",1)
        ).andExpect(MockMvcResultMatchers.status().isOk());

        Mockito.verify(danceSchoolService,Mockito.atLeastOnce()).deleteById(1);
    }

    @Test
    void findByName() throws Exception {
        DanceSchool toReturn = new DanceSchool("Best dance school", 3, null, null);
        ReflectionTestUtils.setField(toReturn, "id", 1);
        BDDMockito.given(danceSchoolService.findByName(toReturn.getName())).willReturn(toReturn);

        mockMvc.perform(
                MockMvcRequestBuilders.get("/api/v1/schools/find?name={name}", toReturn.getName())
        ).andExpect(MockMvcResultMatchers.status().isOk())
         .andExpect(MockMvcResultMatchers.jsonPath("$.name",CoreMatchers.is(toReturn.getName())))
         .andExpect(MockMvcResultMatchers.jsonPath("$.id",CoreMatchers.is(toReturn.getId())));

        Mockito.verify(danceSchoolService,Mockito.atLeastOnce()).findByName(toReturn.getName());
    }

    @Test
    void addTrainer() throws Exception {
        DanceSchoolDTO toReturn = new DanceSchoolDTO(1, "Best dance school", 3, null, Arrays.asList(2));
        BDDMockito.given(danceSchoolService.addTrainer(1,2)).willReturn(toReturn);

        mockMvc.perform(
                MockMvcRequestBuilders.put("/api/v1/schools/{id}/addTrainer?trainerId={trainerId}", toReturn.getId(),2)
        ).andExpect(MockMvcResultMatchers.status().isOk())
         .andExpect(MockMvcResultMatchers.jsonPath("$.trainersIds", CoreMatchers.is(toReturn.getTrainersIds())));

        Mockito.verify(danceSchoolService,Mockito.atLeastOnce()).addTrainer(1,2);
    }

    @Test
    void addDancer() throws Exception {
        DanceSchoolDTO toReturn = new DanceSchoolDTO(1, "Best dance school", 3, Arrays.asList(2),null);

        BDDMockito.given(danceSchoolService.addDancer(1,2)).willReturn(toReturn);

        mockMvc.perform(
                MockMvcRequestBuilders.put("/api/v1/schools/{id}/addDancer?dancerId={dancerId}", toReturn.getId(),2)
        ).andExpect(MockMvcResultMatchers.status().isOk())
         .andExpect(MockMvcResultMatchers.jsonPath("$.dancersIds", CoreMatchers.is(toReturn.getDancersIds())));

        Mockito.verify(danceSchoolService,Mockito.atLeastOnce()).addDancer(1,2);
    }


}
