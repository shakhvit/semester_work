package cz.cvut.fit.shakhvit.service;

import cz.cvut.fit.shakhvit.dto.DanceSchoolDTO;
import cz.cvut.fit.shakhvit.dto.DancerCreateDTO;
import cz.cvut.fit.shakhvit.dto.DancerDTO;
import cz.cvut.fit.shakhvit.entity.DanceSchool;
import cz.cvut.fit.shakhvit.entity.Dancer;
import cz.cvut.fit.shakhvit.entity.Trainer;
import cz.cvut.fit.shakhvit.exceptions.ServerException;
import cz.cvut.fit.shakhvit.repository.DancerRepository;
import org.junit.jupiter.api.*;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@TestInstance( TestInstance.Lifecycle.PER_CLASS )
class DancerServiceTest {

  @Autowired
  private DancerService dancerService;

  @MockBean
  private DancerRepository dancerRepository;

  @MockBean
  private DanceSchoolService danceSchoolService;

  @MockBean
  private TrainerService trainerService;

  @Test
  void findById() throws ServerException{
    Dancer dancerToReturn1 = new Dancer (
            "Vitalii", "Shakhmatov", null, null
    );
    ReflectionTestUtils.setField( dancerToReturn1, "id", 1 );
    BDDMockito.given( dancerRepository.findById( 1 ) ).willReturn( Optional.of( dancerToReturn1 ) );

    Dancer returned = dancerService.findById( 1 );
    Assertions.assertEquals( dancerToReturn1, returned );

    Mockito.verify( dancerRepository, Mockito.atLeastOnce() ).findById( 1 );
  }

  @Test
  void findByIds() throws ServerException{
    Dancer dancerToReturn1 = new Dancer (
            "Vitalii", "Shakhmatov", null, null
    );
    Dancer dancerToReturn2 = new Dancer (
            "Jan", "Novotny", null, null
    );
    Dancer dancerToReturn3 = new Dancer (
            "Marek", "Bures", null, null
    );
    ReflectionTestUtils.setField( dancerToReturn1, "id", 1 );
    ReflectionTestUtils.setField( dancerToReturn2, "id", 2 );
    ReflectionTestUtils.setField( dancerToReturn3, "id", 3 );
    List<Dancer> dancersToReturn = Arrays.asList( dancerToReturn1, dancerToReturn2, dancerToReturn3 );
    List<Integer> ids = Arrays.asList(1,2,3);
    BDDMockito.given( dancerRepository.findAllById( ids )).willReturn( dancersToReturn );


    List<Dancer> dancers = dancerService.findByIds( ids );
    Assertions.assertEquals( dancers, dancersToReturn );

    Mockito.verify( dancerRepository, Mockito.atLeastOnce() ).findAllById( ids );
  }

  @Test
  void findAll() {
    Dancer dancerToReturn1 = new Dancer (
            "Vitalii", "Shakhmatov", null, null
    );
    Dancer dancerToReturn2 = new Dancer (
            "Jan", "Novotny", null, null
    );
    Dancer dancerToReturn3 = new Dancer (
            "Marek", "Bures", null, null
    );
    ReflectionTestUtils.setField( dancerToReturn1, "id", 1 );
    ReflectionTestUtils.setField( dancerToReturn2, "id", 2 );
    ReflectionTestUtils.setField( dancerToReturn3, "id", 3 );
    List<Dancer> dancersToReturn = Arrays.asList( dancerToReturn1, dancerToReturn2, dancerToReturn3 );
    BDDMockito.given( dancerRepository.findAll() ).willReturn( dancersToReturn );
    List<Dancer> dancers = dancerService.findAll();
    Assertions.assertEquals( dancers, dancersToReturn );
    Mockito.verify( dancerRepository, Mockito.atLeastOnce() ).findAll();
  }


  @Test
  void create() throws ServerException{
    DancerCreateDTO dancerCreateDTO = new DancerCreateDTO(
            "Vitalii", "Shakhmatov", null, null
    );


    DancerDTO created = dancerService.create( dancerCreateDTO );
    DancerCreateDTO withoutID = new DancerCreateDTO( created.getFirstName(),
            created.getLastName(),
            created.getDanceSchoolsId(),
            created.getTrainersIds() );
    Assertions.assertEquals( dancerCreateDTO, withoutID );

    Dancer check = new Dancer(
            "Vitalii", "Shakhmatov", null, null
    );
    ReflectionTestUtils.setField( check, "id", created.getId() );

    Mockito.verify( dancerRepository, Mockito.atLeastOnce() ).save( check );
  }

  @Test
  void update() {
    Dancer dancerToReturn1 = new Dancer (
            "Vitalii", "Shakhmatov", null, null
    );
    ReflectionTestUtils.setField( dancerToReturn1, "id", 1 );
    BDDMockito.given( dancerRepository.findById( 1 ) ).willReturn( Optional.of( dancerToReturn1 ) );

    DancerCreateDTO dancerUpdate = new DancerCreateDTO (
            "Unique name",
            "Unique surname", null, null
    );

    DancerDTO updated = new DancerDTO(
            1,
            dancerUpdate.getFirstName(),
            dancerUpdate.getLastName(),
            dancerUpdate.getDanceSchoolId(),
            dancerUpdate.getTrainersIds()
    );
    try {
      DancerDTO returned = dancerService.update( 1, dancerUpdate );
      Assertions.assertEquals( returned, updated );
    } catch( ServerException e ) {
      Assertions.assertTrue( false );
    }
    Mockito.verify( dancerRepository, Mockito.atLeastOnce() ).findById( 1 );
  }

  @Test
  void changeDanceSchool() throws ServerException{
    Dancer dancerToReturn2 = new Dancer (
            "Jan", "Novotny", null, null
    );
    ReflectionTestUtils.setField( dancerToReturn2, "id", 2 );
    BDDMockito.given( dancerRepository.findById( 2 ) ).willReturn( Optional.of( dancerToReturn2 ) );

    DanceSchool danceSchool = new DanceSchool(
            "Best dance school", 3, null, null
    );
    ReflectionTestUtils.setField( danceSchool, "id", 11 );
    BDDMockito.given( danceSchoolService.findById( 11 )).willReturn( danceSchool );

    DancerDTO returnExp = new DancerDTO(
            dancerToReturn2.getId(),
            dancerToReturn2.getFirstName(),
            dancerToReturn2.getLastName(),
            11, null
    );

    DancerDTO returned = dancerService.changeDanceSchool( 2, danceSchool.getId() );
    Assertions.assertEquals( returned, returnExp );

    Mockito.verify( dancerRepository, Mockito.atLeastOnce() ).findById( 2 );
    Mockito.verify( danceSchoolService, Mockito.atLeastOnce() ).findById( danceSchool.getId() );
  }

  @Test
  void addTrainer() throws ServerException{
    Dancer dancerToReturn1 = new Dancer (
            "Vitalii", "Shakhmatov", null, null
    );
    ReflectionTestUtils.setField( dancerToReturn1, "id", 1 );
    BDDMockito.given( dancerRepository.findById( 1 ) ).willReturn( Optional.of( dancerToReturn1 ) );

    Trainer trainer = new Trainer (
            "Abc", "Def", null, null
    );
    ReflectionTestUtils.setField( trainer, "id", 21 );
    BDDMockito.given( trainerService.findById( 21 ) ).willReturn( trainer );

    DancerDTO returnExp = new DancerDTO(
            dancerToReturn1.getId(),
            dancerToReturn1.getFirstName(),
            dancerToReturn1.getLastName(),
            null, Arrays.asList( 21 )
    );

    DancerDTO returned = dancerService.addTrainer( 1, trainer.getId() );
    Assertions.assertEquals( returned, returnExp );

    Mockito.verify( dancerRepository, Mockito.atLeastOnce() ).findById( 1 );
    Mockito.verify( trainerService, Mockito.atLeastOnce() ).findById( trainer.getId() );
  }

  @Test
  void delete() throws ServerException{
    Dancer dancerToReturn1 = new Dancer (
            "Vitalii", "Shakhmatov", null, null
    );
    ReflectionTestUtils.setField( dancerToReturn1, "id", 1 );
    BDDMockito.given( dancerRepository.findById( 1 ) ).willReturn( Optional.of( dancerToReturn1 ) );

    dancerService.deleteById( 1 );
    Mockito.verify( dancerRepository, Mockito.atLeastOnce() ).deleteById( 1 );
  }

}