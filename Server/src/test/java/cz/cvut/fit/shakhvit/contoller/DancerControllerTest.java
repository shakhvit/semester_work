package cz.cvut.fit.shakhvit.contoller;

import cz.cvut.fit.shakhvit.dto.DancerCreateDTO;
import cz.cvut.fit.shakhvit.dto.DancerDTO;
import cz.cvut.fit.shakhvit.entity.Dancer;
import cz.cvut.fit.shakhvit.exceptions.ServerException;
import cz.cvut.fit.shakhvit.service.DancerService;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockBeans;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@AutoConfigureMockMvc
class DancerControllerTest {


    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DancerService dancerService;

    @Test
    void create() throws  Exception {
        DancerDTO dancerDTO = new DancerDTO( 1, "Vitalii", "Shakhmatov",null,null);
        DancerCreateDTO dancerToCreate = new DancerCreateDTO("Vitalii", "Shakhmatov", null, null);
        BDDMockito.given(dancerService.create(dancerToCreate)).willReturn(dancerDTO);

        mockMvc.perform(
                MockMvcRequestBuilders
                .post("/api/v1/dancers")
                .contentType("application/json")
                .content("{ \"firstName\": \"Vitalii\", \"lastName\": \"Shakhmatov\" }")
        ).andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.header().exists("Location"));
    }

    @Test
    void readOne() throws Exception {
        Dancer dancer = new Dancer( "Vitalii", "Shakhmatov",null,null);
        ReflectionTestUtils.setField(dancer, "id", 1);

        BDDMockito.given(dancerService.findById(1)).willReturn(dancer);

        mockMvc.perform(
                MockMvcRequestBuilders.get("/api/v1/dancers/{id}", dancer.getId())
                .accept("application/json")
                .contentType("application/json")
        ).andExpect(MockMvcResultMatchers.status().isOk())
         .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.firstName", CoreMatchers.is(dancer.getFirstName())))
            .andExpect(MockMvcResultMatchers.jsonPath("$.lastName", CoreMatchers.is(dancer.getLastName())))
            .andExpect(MockMvcResultMatchers.jsonPath("$._links.self.href", CoreMatchers.endsWith("/api/v1/dancers/1")));

        Mockito.verify(dancerService, Mockito.atLeastOnce()).findById(1);
    }

    @Test
    void readAll() throws Exception{
        Dancer dancer1 = new Dancer("Vitalii","Shakhmatov",null,null);
        Dancer dancer2 = new Dancer("Marek", "Bures", null,null);
        ReflectionTestUtils.setField(dancer1, "id", 1);
        ReflectionTestUtils.setField(dancer2, "id", 2);

        DancerDTO toReturn1DTO = new DancerDTO(dancer1.getId(), dancer1.getFirstName(), dancer1.getLastName(), null, null);
        DancerDTO toReturn2DTO = new DancerDTO(dancer2.getId(), dancer2.getFirstName(), dancer2.getLastName(), null, null);

        BDDMockito.given(dancerService.findAll()).willReturn(List.of(dancer1, dancer2));

        mockMvc.perform(
                MockMvcRequestBuilders.get("/api/v1/dancers/")
        ).andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.dancerDTOList.[0].id", CoreMatchers.is(toReturn1DTO.getId())))
            .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.dancerDTOList.[0].firstName", CoreMatchers.is(toReturn1DTO.getFirstName())))
            .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.dancerDTOList.[0].lastName", CoreMatchers.is(toReturn1DTO.getLastName())))
            .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.dancerDTOList.[0]._links.self.href", CoreMatchers.endsWith("/api/v1/dancers/1")))
            .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.dancerDTOList.[1].id", CoreMatchers.is(toReturn2DTO.getId())))
            .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.dancerDTOList.[1].firstName", CoreMatchers.is(toReturn2DTO.getFirstName())))
            .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.dancerDTOList.[1].lastName", CoreMatchers.is(toReturn2DTO.getLastName())))
            .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.dancerDTOList.[1]._links.self.href", CoreMatchers.endsWith("/api/v1/dancers/2")));

        Mockito.verify(dancerService,Mockito.atLeastOnce()).findAll();
    }

    @Test
    void update() throws Exception{
        DancerCreateDTO dancerToUpdate = new DancerCreateDTO("Marek", "Bures", null, null);
        DancerDTO updated = new DancerDTO(1, "Marek", "Bures", null, null);
        BDDMockito.given(dancerService.update(1, dancerToUpdate)).willReturn(updated);

        mockMvc.perform(
                MockMvcRequestBuilders.put("/api/v1/dancers/{id}", updated.getId())
                .accept("application/json")
                .contentType("application/json")
                .content("{\"firstName\": \"Marek\", \"lastName\": \"Bures\"}")
        ).andExpect(MockMvcResultMatchers.status().isAccepted())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(updated.getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName", CoreMatchers.is(updated.getFirstName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName", CoreMatchers.is(updated.getLastName())));

        Mockito.verify(dancerService,Mockito.atLeastOnce()).update(1,dancerToUpdate);
    }

    @Test
    void delete() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.delete("/api/v1/dancers/1")
        ).andExpect(MockMvcResultMatchers.status().isOk());

        Mockito.verify(dancerService,Mockito.atLeastOnce()).deleteById(1);
    }

    @Test
    void changeDanceSchool() throws Exception {
        DancerDTO changed = new DancerDTO(1, "Vitalii", "Shakhmatov", 2, null);
        BDDMockito.given(dancerService.changeDanceSchool(1, 2)).willReturn(changed);

        mockMvc.perform(
                MockMvcRequestBuilders.put("/api/v1/dancers/{id}/changeSchool?schoolId={schoolId}",changed.getId(), changed.getDanceSchoolsId())
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(changed.getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName", CoreMatchers.is(changed.getFirstName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName", CoreMatchers.is(changed.getLastName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.danceSchoolsId", CoreMatchers.is(changed.getDanceSchoolsId())));

        Mockito.verify(dancerService,Mockito.atLeastOnce()).changeDanceSchool(changed.getId(),changed.getDanceSchoolsId());

    }

    @Test
    void addTrainer() throws Exception{
        DancerDTO changed = new DancerDTO(1, "Vitalii", "Shakhmatov", null, Arrays.asList(2));
        BDDMockito.given(dancerService.addTrainer(1,2)).willReturn(changed);

        mockMvc.perform(
                MockMvcRequestBuilders.put("/api/v1/dancers/{id}/addTrainer?trainerId={trainerId}",changed.getId(),2)
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(changed.getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName", CoreMatchers.is(changed.getFirstName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName", CoreMatchers.is(changed.getLastName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.trainersIds", CoreMatchers.is(changed.getTrainersIds())));

        Mockito.verify(dancerService,Mockito.atLeastOnce()).addTrainer(1,2);
    }
}