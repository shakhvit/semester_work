package cz.cvut.fit.shakhvit.service;

import cz.cvut.fit.shakhvit.dto.DancerDTO;
import cz.cvut.fit.shakhvit.dto.TrainerCreateDTO;
import cz.cvut.fit.shakhvit.dto.TrainerDTO;
import cz.cvut.fit.shakhvit.entity.DanceSchool;
import cz.cvut.fit.shakhvit.entity.Dancer;
import cz.cvut.fit.shakhvit.entity.Trainer;
import cz.cvut.fit.shakhvit.exceptions.ServerException;
import cz.cvut.fit.shakhvit.repository.TrainerRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class TrainerServiceTest {

  @Autowired
  private TrainerService trainerService;

  @MockBean
  private TrainerRepository trainerRepository;

  @MockBean
  private DanceSchoolService danceSchoolService;

  @MockBean
  private DancerService dancerService;

  @Test
  void findById() throws ServerException{
    Trainer trainer = new Trainer(
            "Vitalii",
            "Shakhmatov",
            null, null
    );
    ReflectionTestUtils.setField( trainer, "id", 1 );
    BDDMockito.given( trainerRepository.findById( 1 ) ).willReturn( Optional.of( trainer ) );

    Trainer returned = trainerService.findById( 1 );
    Assertions.assertEquals( returned, trainer );

    Mockito.verify( trainerRepository, Mockito.atLeastOnce() ).findById( 1 );
  }

  @Test
  void findByIds() throws ServerException{
    Trainer trainer1 = new Trainer(
            "Vitalii",
            "Shakhmatov",
            null, null
    );
    ReflectionTestUtils.setField( trainer1, "id", 1 );
    Trainer trainer2 = new Trainer(
            "Milos",
            "Zeman",
            null, null
    );
    ReflectionTestUtils.setField( trainer2, "id", 2 );
    Trainer trainer3 = new Trainer(
            "Ladislav",
            "Vagner",
            null, null
    );
    ReflectionTestUtils.setField( trainer3, "id", 3 );
    List<Integer> ids = Arrays.asList(1,2,3);
    List<Trainer> trainers = Arrays.asList( trainer1, trainer2, trainer3 );
    BDDMockito.given( trainerRepository.findAllById( ids ) ).willReturn( trainers );

    List<Trainer> returned = trainerService.findByIds( ids );
    Assertions.assertEquals( trainers, returned );
    Mockito.verify( trainerRepository, Mockito.atLeastOnce() ).findAllById( ids );
  }

  @Test
  void findAll() {
    Trainer trainer1 = new Trainer(
            "Vitalii",
            "Shakhmatov",
            null, null
    );
    ReflectionTestUtils.setField( trainer1, "id", 1 );
    Trainer trainer2 = new Trainer(
            "Milos",
            "Zeman",
            null, null
    );
    ReflectionTestUtils.setField( trainer2, "id", 2 );
    Trainer trainer3 = new Trainer(
            "Ladislav",
            "Vagner",
            null, null
    );
    ReflectionTestUtils.setField( trainer3, "id", 3 );
    List<Integer> ids = Arrays.asList(1,2,3);
    List<Trainer> trainers = Arrays.asList( trainer1, trainer2, trainer3 );
    BDDMockito.given( trainerRepository.findAll() ).willReturn( trainers );

    List<Trainer> returned = trainerService.findAll();
    Assertions.assertEquals( trainers, returned );

    Mockito.verify( trainerRepository, Mockito.atLeastOnce() ).findAll();
  }

  @Test
  void update() throws ServerException{
    Trainer trainer = new Trainer(
            "Vitalii",
            "Shakhmatov",
            null, null
    );
    ReflectionTestUtils.setField( trainer, "id", 1 );
    BDDMockito.given( trainerRepository.findById( 1 ) ).willReturn( Optional.of( trainer ) );

    TrainerCreateDTO trainerCreateDTO = new TrainerCreateDTO(
            "Ladislav", "Vagner", null, null
    );

    TrainerDTO cmp = new TrainerDTO(
            1,
            trainerCreateDTO.getFirstName(),
            trainerCreateDTO.getLastName(),
            trainerCreateDTO.getDanceSchoolsIds(),
            trainerCreateDTO.getDancersIds()
    );

    TrainerDTO returned = trainerService.update( 1, trainerCreateDTO );
    Assertions.assertEquals( cmp, returned );

    Mockito.verify(trainerRepository, Mockito.atLeastOnce()).findById(1);
  }

  @Test
  void delete() throws ServerException{
    Trainer trainer = new Trainer(
            "Vitalii",
            "Shakhmatov",
            null, null
    );
    ReflectionTestUtils.setField( trainer, "id", 1 );
    BDDMockito.given( trainerRepository.findById( 1 ) ).willReturn( Optional.of( trainer ) );

    trainerService.deleteById( 1 );

    Mockito.verify( trainerRepository, Mockito.atLeastOnce() ).deleteById( 1 );
  }

  @Test
  void create() throws ServerException{
    TrainerCreateDTO trainerCreateDTO = new TrainerCreateDTO(
            "Vitalii",
            "Shakhmatov",
            null, null
    );

    TrainerDTO returned = trainerService.create( trainerCreateDTO );
    Assertions.assertEquals( returned.getFirstName(), trainerCreateDTO.getFirstName() );
    Assertions.assertEquals( returned.getLastName(), trainerCreateDTO.getLastName() );
    Assertions.assertEquals( returned.getDancersIds(), trainerCreateDTO.getDancersIds() );
    Assertions.assertEquals( returned.getDanceSchoolsIds(), trainerCreateDTO.getDanceSchoolsIds() );

    Trainer check = new Trainer("Vitalii", "Shakhmatov", null, null);
    ReflectionTestUtils.setField(check, "id", returned.getId());

    Mockito.verify(trainerRepository, Mockito.atLeastOnce()).save(check);
  }

  @Test
  void addDanceSchool() throws ServerException {
    Trainer trainer = new Trainer(
            "Vitalii",
            "Shakhmatov",
            null, null
    );
    ReflectionTestUtils.setField( trainer, "id", 1 );
    BDDMockito.given( trainerRepository.findById( 1 ) ).willReturn( Optional.of( trainer ) );

    DanceSchool danceSchool = new DanceSchool(
            "Best dance school", 3, null, null
    );
    ReflectionTestUtils.setField( danceSchool, "id", 2 );
    BDDMockito.given( danceSchoolService.findById( 2 ) ).willReturn( danceSchool );

    TrainerDTO expected = new TrainerDTO(
            1,
            trainer.getFirstName(),
            trainer.getLastName(),
            Arrays.asList( 2 ),
            null
    );

    TrainerDTO returned = trainerService.addDanceSchool( 1, 2 );
    Assertions.assertEquals( expected, returned );
    Mockito.verify( trainerRepository, Mockito.atLeastOnce() ).findById( 1 );
    Mockito.verify( danceSchoolService, Mockito.atLeastOnce() ).findById( 2 );
  }

  @Test
  void addDancer() throws ServerException{
    Trainer trainer = new Trainer(
            "Vitalii",
            "Shakhmatov",
            null, null
    );
    ReflectionTestUtils.setField( trainer, "id", 1 );

    Dancer dancer = new Dancer(
            "Test",
            "Dancer",
            null, null
    );
    ReflectionTestUtils.setField( dancer, "id", 2 );

    BDDMockito.given( trainerRepository.findById( 1 ) ).willReturn( Optional.of( trainer ) );
    BDDMockito.given( dancerService.findById( 2 ) ).willReturn(  dancer );

    TrainerDTO expected = new TrainerDTO(
            1,
            trainer.getFirstName(),
            trainer.getLastName(),
            null,
            Arrays.asList( 2 )
    );

    TrainerDTO returned = trainerService.addDancer( 1, 2 );
    Assertions.assertEquals( returned, expected );
    Mockito.verify( trainerRepository, Mockito.atLeastOnce() ).findById( 1 );
    Mockito.verify( dancerService, Mockito.atLeastOnce() ).findById( 2 );
  }

  @Test
  void getPotentialDancers() throws ServerException{
    Trainer trainer = new Trainer(
            "Vitalii",
            "Shakhmatov",
            null, null
    );
    ReflectionTestUtils.setField( trainer, "id", 1 );

    DanceSchool school = new DanceSchool(
            "Best dance school", 3, null, null
    );
    ReflectionTestUtils.setField(school, "id", 2);

    Dancer dancer = new Dancer("Marek", "Bures", null, null);
    ReflectionTestUtils.setField(dancer, "id", 3);
    school.setDancers(Arrays.asList(dancer));
    dancer.setDanceSchool(school);
    trainer.setDanceSchools(Arrays.asList(school));
    school.setTrainers(Arrays.asList(trainer));

    DancerDTO dancerDTO = new DancerDTO(3, "Marek", "Bures", 2, null);

    BDDMockito.given( trainerRepository.findById( 1 ) ).willReturn( Optional.of( trainer ) );

    List<Dancer> dancers = trainerService.getPotentialDancers(1);
    Assertions.assertEquals(dancers.size(), 1);
    Assertions.assertEquals(dancers.get(0).getFirstName(), "Marek");
    Assertions.assertEquals(dancers.get(0).getLastName(), "Bures");
    Assertions.assertEquals(dancers.get(0).getId(), 3);
    Assertions.assertEquals(dancers.get(0), dancer);

    Mockito.verify(trainerRepository, Mockito.atLeastOnce()).findById(1);

  }

}
