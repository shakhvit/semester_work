package cz.cvut.fit.shakhvit.contoller;

import cz.cvut.fit.shakhvit.dto.DancerDTO;
import cz.cvut.fit.shakhvit.dto.TrainerCreateDTO;
import cz.cvut.fit.shakhvit.dto.TrainerDTO;
import cz.cvut.fit.shakhvit.entity.Dancer;
import cz.cvut.fit.shakhvit.entity.Trainer;
import cz.cvut.fit.shakhvit.service.TrainerService;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockitoTestExecutionListener;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;

@SpringBootTest
@AutoConfigureMockMvc
public class TrainerControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private TrainerService trainerService;

    @Test
    void create() throws Exception {
        TrainerDTO toReturn = new TrainerDTO(1, "Vitalii", "Shakhmatov", null, null);
        TrainerCreateDTO toCreate = new TrainerCreateDTO(
                toReturn.getFirstName(),
                toReturn.getLastName(),
                toReturn.getDanceSchoolsIds(),
                toReturn.getDancersIds()
        );

        BDDMockito.given(trainerService.create(toCreate)).willReturn(toReturn);

        mvc.perform(MockMvcRequestBuilders.post("/api/v1/trainers/")
                .contentType("application/json")
                .content("{ \"firstName\": \"Vitalii\", \"lastName\": \"Shakhmatov\" }")
        ).andExpect(MockMvcResultMatchers.status().isCreated())
         .andExpect(MockMvcResultMatchers.header().exists("Location"))
         .andExpect(MockMvcResultMatchers.jsonPath("$._links.self.href", CoreMatchers.endsWith("api/v1/trainers/1")));

        Mockito.verify(trainerService,Mockito.atLeastOnce()).create(toCreate);
    }

    @Test
    void update() throws Exception {
        TrainerDTO toReturn = new TrainerDTO(1, "Vitalii", "Shakhmatov", null, null );
        TrainerCreateDTO toUpdate = new TrainerCreateDTO("Vitalii", "Shakhmatov", null, null );
        BDDMockito.given(trainerService.update(1, toUpdate)).willReturn(toReturn);

        mvc.perform(
                MockMvcRequestBuilders.put("/api/v1/trainers/{id}", toReturn.getId())
                .contentType("application/json")
                .content(" { \"firstName\": \"Vitalii\", \"lastName\": \"Shakhmatov\"  }  " )
        ).andExpect(MockMvcResultMatchers.status().isOk())
         .andExpect(MockMvcResultMatchers.jsonPath("$.firstName", CoreMatchers.is(toReturn.getFirstName())))
         .andExpect(MockMvcResultMatchers.jsonPath("$.lastName", CoreMatchers.is(toReturn.getLastName())))
         .andExpect(MockMvcResultMatchers.jsonPath("$._links.self.href", CoreMatchers.endsWith("/api/v1/trainers/1")));

        Mockito.verify(trainerService,Mockito.atLeastOnce()).update(1, toUpdate);

    }

    @Test
    void delete() throws  Exception {
        mvc.perform(MockMvcRequestBuilders.delete("/api/v1/trainers/{id}", 1))
                .andExpect(MockMvcResultMatchers.status().isOk());

        Mockito.verify(trainerService,Mockito.atLeastOnce()).deleteById(1);
    }

    @Test
    void findOne() throws  Exception {
        Trainer toReturn = new Trainer(  "Vitalii", "Shakhmatov", null, null );
        ReflectionTestUtils.setField(toReturn, "id", 1);

        BDDMockito.given(trainerService.findById(toReturn.getId())).willReturn(toReturn);

        mvc.perform(
                MockMvcRequestBuilders.get("/api/v1/trainers/{id}", toReturn.getId())
        ).andExpect(MockMvcResultMatchers.status().isOk())
         .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(toReturn.getId())))
         .andExpect(MockMvcResultMatchers.jsonPath("$.firstName", CoreMatchers.is(toReturn.getFirstName())))
         .andExpect(MockMvcResultMatchers.jsonPath("$.lastName", CoreMatchers.is(toReturn.getLastName())))
         .andExpect(MockMvcResultMatchers.jsonPath("$._links.self.href", CoreMatchers.endsWith("/api/v1/trainers/1")));


        Mockito.verify(trainerService, Mockito.atLeastOnce()).findById(toReturn.getId());
    }

    @Test
    void findAll() throws  Exception {
        Trainer toReturn1 = new Trainer( "Vitalii", "Shakhmatov", null, null );
        Trainer toReturn2 = new Trainer( "Marek", "Bures", null, null );
        ReflectionTestUtils.setField(toReturn1, "id", 1);
        ReflectionTestUtils.setField(toReturn2, "id", 2);

        TrainerDTO toReturn1DTO = new TrainerDTO(toReturn1.getId(), toReturn1.getFirstName(), toReturn1.getLastName(), null, null);
        TrainerDTO toReturn2DTO = new TrainerDTO(toReturn2.getId(), toReturn2.getFirstName(), toReturn2.getLastName(), null, null);

        BDDMockito.given(trainerService.findAll()).willReturn(Arrays.asList(toReturn1, toReturn2));

        mvc.perform(
                MockMvcRequestBuilders.get("/api/v1/trainers/")
        ).andExpect(MockMvcResultMatchers.status().isOk())
         .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.trainerDTOList.[0].id", CoreMatchers.is(toReturn1DTO.getId())))
         .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.trainerDTOList.[0].firstName", CoreMatchers.is(toReturn1DTO.getFirstName())))
         .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.trainerDTOList.[0].lastName", CoreMatchers.is(toReturn1DTO.getLastName())))
         .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.trainerDTOList.[0]._links.self.href", CoreMatchers.endsWith("/api/v1/trainers/1")))
         .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.trainerDTOList.[1].id", CoreMatchers.is(toReturn2DTO.getId())))
         .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.trainerDTOList.[1].firstName", CoreMatchers.is(toReturn2DTO.getFirstName())))
         .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.trainerDTOList.[1].lastName", CoreMatchers.is(toReturn2DTO.getLastName())))
         .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.trainerDTOList.[1]._links.self.href", CoreMatchers.endsWith("/api/v1/trainers/2")));


        Mockito.verify(trainerService,Mockito.atLeastOnce()).findAll();

    }

    @Test
    void addDancer() throws  Exception {
        TrainerDTO toReturn = new TrainerDTO( 1, "Vitalii", "Shakhmatov", null, Arrays.asList(2) );

        BDDMockito.given(trainerService.addDancer(1,2)).willReturn(toReturn);

        mvc.perform(
                MockMvcRequestBuilders.put("/api/v1/trainers/{id}/addDancer?dancerId={dancerI}",1,2)
        ).andExpect(MockMvcResultMatchers.status().isOk())
         .andExpect(MockMvcResultMatchers.jsonPath("$.firstName", CoreMatchers.is(toReturn.getFirstName())))
         .andExpect(MockMvcResultMatchers.jsonPath("$.lastName", CoreMatchers.is(toReturn.getLastName())))
         .andExpect(MockMvcResultMatchers.jsonPath("$.dancersIds", CoreMatchers.is(toReturn.getDancersIds())))
         .andExpect(MockMvcResultMatchers.jsonPath("$._links.self.href", CoreMatchers.endsWith("/api/v1/trainers/1")));

        Mockito.verify(trainerService,Mockito.atLeastOnce()).addDancer(1,2);
    }

    @Test
    void addDanceSchool() throws  Exception {
        TrainerDTO toReturn = new TrainerDTO( 1, "Vitalii", "Shakhmatov", Arrays.asList(2), null );

        BDDMockito.given(trainerService.addDanceSchool(1,2)).willReturn(toReturn);

        mvc.perform(
                MockMvcRequestBuilders.put("/api/v1/trainers/{id}/addSchool?schoolId={schoolId}",1,2)
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName", CoreMatchers.is(toReturn.getFirstName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName", CoreMatchers.is(toReturn.getLastName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.danceSchoolsIds", CoreMatchers.is(toReturn.getDanceSchoolsIds())));

        Mockito.verify(trainerService, Mockito.atLeastOnce()).addDanceSchool(1,2);
    }

    @Test
    void getPotentialDancers() throws  Exception {
        Dancer dancer1 = new Dancer(  "Vitalii", "Shakhmatov", null, null );
        Dancer dancer2 = new Dancer( "Marek", "Bures", null, null );
        ReflectionTestUtils.setField(dancer1, "id", 1);
        ReflectionTestUtils.setField(dancer2, "id", 2);

        DancerDTO toReturn1DTO = new DancerDTO(dancer1.getId(), dancer1.getFirstName(), dancer1.getLastName(), null, null);
        DancerDTO toReturn2DTO = new DancerDTO(dancer2.getId(), dancer2.getFirstName(), dancer2.getLastName(), null, null);

        BDDMockito.given(trainerService.getPotentialDancers(3)).willReturn(Arrays.asList(dancer1,dancer2));

        mvc.perform(
                MockMvcRequestBuilders.get("/api/v1/trainers/{id}/getPotentialDancers", 3)
        ).andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.dancerDTOList.[0].id", CoreMatchers.is(toReturn1DTO.getId())))
            .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.dancerDTOList.[0].firstName", CoreMatchers.is(toReturn1DTO.getFirstName())))
            .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.dancerDTOList.[0].lastName", CoreMatchers.is(toReturn1DTO.getLastName())))
            .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.dancerDTOList.[0]._links.self.href", CoreMatchers.endsWith("/api/v1/dancers/1")))
            .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.dancerDTOList.[1].id", CoreMatchers.is(toReturn2DTO.getId())))
            .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.dancerDTOList.[1].firstName", CoreMatchers.is(toReturn2DTO.getFirstName())))
            .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.dancerDTOList.[1].lastName", CoreMatchers.is(toReturn2DTO.getLastName())))
            .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.dancerDTOList.[1]._links.self.href", CoreMatchers.endsWith("/api/v1/dancers/2")));

        Mockito.verify(trainerService,Mockito.atLeastOnce()).getPotentialDancers(3);

    }
}

