package cz.cvut.fit.shakhvit.service;

import cz.cvut.fit.shakhvit.dto.DanceSchoolCreateDTO;
import cz.cvut.fit.shakhvit.dto.DanceSchoolDTO;
import cz.cvut.fit.shakhvit.entity.DanceSchool;
import cz.cvut.fit.shakhvit.entity.Dancer;
import cz.cvut.fit.shakhvit.entity.Trainer;
import cz.cvut.fit.shakhvit.exceptions.ServerException;
import cz.cvut.fit.shakhvit.repository.DanceSchoolRepository;
import org.apache.catalina.Server;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class DanceSchoolTest {
  @Autowired
  private DanceSchoolService danceSchoolService;

  @MockBean
  private DanceSchoolRepository danceSchoolRepository;

  @MockBean
  private TrainerService trainerService;

  @MockBean
  private DancerService dancerService;

  @Test
  void create() throws ServerException{
    DanceSchoolCreateDTO danceSchoolCreateDTO = new DanceSchoolCreateDTO(
            "Best dance school", 3, null, null
    );
    DanceSchool danceSchoolToReturn = new DanceSchool(
            danceSchoolCreateDTO.getName(), danceSchoolCreateDTO.getNumberOfHalls(),
            null, null
    );

    DanceSchoolDTO returned = danceSchoolService.create( danceSchoolCreateDTO );
    Assertions.assertEquals( returned.getName(), danceSchoolToReturn.getName() );
    Assertions.assertEquals( returned.getNumberOfHalls(), danceSchoolToReturn.getNumberOfHalls() );

    Mockito.verify( danceSchoolRepository, Mockito.atLeastOnce() ).save( danceSchoolToReturn );

  }

  @Test
  void update() throws ServerException{
    DanceSchool danceSchoolToReturn1 = new DanceSchool(
            "Best dance school", 1, null, null
    );
    ReflectionTestUtils.setField( danceSchoolToReturn1, "id", 1 );
    BDDMockito.given( danceSchoolRepository.findById( 1 ) ).willReturn( Optional.of( danceSchoolToReturn1 ) );

    DanceSchoolCreateDTO danceSchoolToUpdate = new DanceSchoolCreateDTO(
            "Second best dance school", 2, null, null
    );

    DanceSchoolDTO updated = danceSchoolService.update( 1, danceSchoolToUpdate );
    Assertions.assertEquals( danceSchoolToUpdate.getName(), updated.getName() );
    Assertions.assertEquals( danceSchoolToUpdate.getNumberOfHalls(), updated.getNumberOfHalls() );
    Assertions.assertEquals( danceSchoolToUpdate.getDancersIds(), updated.getDancersIds() );
    Assertions.assertEquals( danceSchoolToUpdate.getTrainersIds(), updated.getTrainersIds() );

    Mockito.verify( danceSchoolRepository, Mockito.atLeastOnce() ).findById( 1 );
  }

  @Test
  void findById() throws ServerException{
    DanceSchool danceSchoolToReturn = new DanceSchool(
            "Best dance school", 3, null, null
    );
    ReflectionTestUtils.setField( danceSchoolToReturn, "id", 1 );
    BDDMockito.given( danceSchoolRepository.findById( 1 ) ).willReturn( Optional.of( danceSchoolToReturn ) );

    DanceSchool returned = danceSchoolService.findById( 1 );
    Assertions.assertEquals( returned, danceSchoolToReturn );

    Mockito.verify( danceSchoolRepository, Mockito.atLeastOnce() ).findById( 1 );
  }

  @Test
  void findByIds() throws ServerException{
    DanceSchool danceSchoolToReturn1 = new DanceSchool(
            "Best dance school", 3, null, null
    );
    ReflectionTestUtils.setField( danceSchoolToReturn1, "id", 1 );
    DanceSchool danceSchoolToReturn2 = new DanceSchool(
            "Best dance school", 3, null, null
    );
    ReflectionTestUtils.setField( danceSchoolToReturn2, "id", 2 );
    DanceSchool danceSchoolToReturn3 = new DanceSchool(
            "Best dance school", 3, null, null
    );
    ReflectionTestUtils.setField( danceSchoolToReturn3, "id", 3 );
    List<DanceSchool> danceSchools = Arrays.asList(
            danceSchoolToReturn1,
            danceSchoolToReturn2,
            danceSchoolToReturn3
    );
    List<Integer> ids = Arrays.asList(1,2,3);
    BDDMockito.given( danceSchoolRepository.findAllById( ids ) ).willReturn( danceSchools );

    List<DanceSchool> returned = danceSchoolService.findByIds( ids );
    Assertions.assertEquals( returned, danceSchools );

  }

  @Test
  void findAll() {
    DanceSchool danceSchoolToReturn1 = new DanceSchool(
            "Best dance school", 3, null, null
    );
    ReflectionTestUtils.setField( danceSchoolToReturn1, "id", 1 );
    DanceSchool danceSchoolToReturn2 = new DanceSchool(
            "Best dance school", 3, null, null
    );
    ReflectionTestUtils.setField( danceSchoolToReturn2, "id", 2 );
    DanceSchool danceSchoolToReturn3 = new DanceSchool(
            "Best dance school", 3, null, null
    );
    ReflectionTestUtils.setField( danceSchoolToReturn3, "id", 3 );
    List<DanceSchool> danceSchools = Arrays.asList(
            danceSchoolToReturn1,
            danceSchoolToReturn2,
            danceSchoolToReturn3
    );
    List<Integer> ids = Arrays.asList(1,2,3);
    BDDMockito.given( danceSchoolRepository.findAll() ).willReturn( danceSchools );

    List<DanceSchool> returned = danceSchoolService.findAll();
    Assertions.assertEquals( returned, danceSchools );

  }

  @Test
  void delete() throws ServerException{
    DanceSchool danceSchoolToReturn = new DanceSchool(
            "Best dance school", 3, null, null
    );
    ReflectionTestUtils.setField( danceSchoolToReturn, "id", 1 );
    BDDMockito.given( danceSchoolRepository.findById( 1 ) ).willReturn( Optional.of( danceSchoolToReturn ) );

    danceSchoolService.deleteById( 1 );

    Mockito.verify( danceSchoolRepository, Mockito.atLeastOnce() ).deleteById( 1 );
  }

  @Test
  void addDancer() throws ServerException{
    DanceSchool danceSchool = new DanceSchool(
            "Best dance school", 3, null, null
    );
    ReflectionTestUtils.setField( danceSchool, "id", 1 );

    BDDMockito.given( danceSchoolRepository.findById( 1 ) ).willReturn( Optional.of( danceSchool ) );

    Dancer dancer = new Dancer(
            "Vitalii",
            "Shakhmatov",
            null, null
    );
    ReflectionTestUtils.setField( dancer, "id", 2 );
    BDDMockito.given( dancerService.findById( 2 ) ).willReturn( dancer );

    DanceSchoolDTO expected = new DanceSchoolDTO(
            danceSchool.getId(),
            danceSchool.getName(),
            danceSchool.getNumberOfHalls(),
            Arrays.asList( 2 ),
            null
    );

    DanceSchoolDTO returned = danceSchoolService.addDancer( 1, 2 );
    Assertions.assertEquals( returned, expected );

    Mockito.verify( danceSchoolRepository, Mockito.atLeastOnce() ).findById( 1 );
    Mockito.verify( dancerService, Mockito.atLeastOnce() ).findById( 2 );
  }

  @Test
  void addTrainer() throws ServerException{
    DanceSchool danceSchool = new DanceSchool(
            "Best dance school", 3, null, null
    );
    ReflectionTestUtils.setField( danceSchool, "id", 1 );

    BDDMockito.given( danceSchoolRepository.findById( 1 ) ).willReturn( Optional.of( danceSchool ) );

    Trainer trainer = new Trainer(
            "Vitalii",
            "Shakhmatov",
            null, null
    );
    ReflectionTestUtils.setField( trainer, "id", 2 );
    BDDMockito.given( trainerService.findById( 2 ) ).willReturn( trainer );

    DanceSchoolDTO expected = new DanceSchoolDTO(
            danceSchool.getId(),
            danceSchool.getName(),
            danceSchool.getNumberOfHalls(),
            null,
            Arrays.asList( 2 )
    );

    DanceSchoolDTO returned = danceSchoolService.addTrainer( 1, 2 );
    Assertions.assertEquals( returned, expected );

    Mockito.verify( danceSchoolRepository, Mockito.atLeastOnce() ).findById( 1 );
    Mockito.verify( trainerService, Mockito.atLeastOnce() ).findById( 2 );
  }

}
