**BUILDING PROJECT: **
1. Clone repository from github
`git clone https://gitlab.fit.cvut.cz/shakhvit/semester_work.git`
2. Go to root directory of the project and run buils.sh script
`./build.sh`


**RUNNING APPLICATION: **
1. Run script to start server
   ` ./run.sh --server`
2. Run script to start client(in another terminal)
    `./run.sh --client`


**TUTORIAL:**
As soon as client is loaded, you will see console with text:
> Welcome!

> Type \help for help

> $(dancer) 

`$` means, that client is waiting for command.
`(dancer)` means, that you are currently working with dancer entity. All commands
as \read, \create, \update etc. would affect dancers. 
There is some usefull commands: 
`\help` - if you need help, run this command and hit enter
`\choose` - this command allow you to choose entity, you want to work with. 
    as soon as you run this command, application will ask you to enter number 
    from 1 to 3 to choose entity. 
`\read `- to read content of database. 
    After \read command is called, application will ask user to choose between 
    two posibilities, either to read all content(user should enter 2), or read 
    only one instance by id(user should choose 1).
`\create` - to create new instance of entity. 
    After \create command is called, application will help the user to provide
    all the required data for create a instance. 
`\update` - to update exact instance of entity. 
    User should provide Id of instance, which they want to update and data to update. 
    Again application will help to fill the data in. 
`\command` - to check which commands is available. 
    Each instance has its own commands. Description of all available commands is below. 
`\exit` - to stop the application and exit. 


**LIST OF AVAILABLE COMMANDS:** 
1. Common commands available for all entities
    a. `\create` arguments: data
        to create new instance
    b. `\read` arguments: id or null
        to read either one instance or all instancies
    c. `\udpate` arguments: id and data
        to update one instance
    d. `\delete` arguments: id
        to delete instance
2. Dancer
    a. `\addTrainer` arguments: dancer id and trainer id
        to add relation between dancer and trainer
    b. `\changeSchool` arguments: dancer id and school id
        to change dance school, which dancer represents
3. Trainer
    a. `\addDancer` arguments: trainer id and dancer id
        to add relation between dancer and trainer
    b. `\addSchool` arguments: trainer id and school id
        to add relation between trainer and school
    c. `\getPotentialDancer` arguments: id
        to get list of all dancers, which trainer could have lecture with. 
        All the dancers, which are in the same schools, where trainer could teach. 
4. Dance school
    a. `\addDancer` arguments: dance school id and dancer id
        to add dance to the school
    b. `\addTrainer` arguments: dance school id and trainer id
        to add relation between dance school and trainer
    c. `\findByName` arguments: name
        to find dance school by its name