#!/bin/bash

echo "Installing dto to maven local" && \
    cd ./dto && ./gradlew install && echo "Done" && \
    echo "Building server" && \
    cd ../Server && ./gradlew build && echo "Done" && \
    echo "Building client" && \
    cd ../rest-client && ./gradlew build && echo "Done" 
