package cz.cvut.fit.shakhvit.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.hateoas.RepresentationModel;

import java.util.List;
import java.util.Objects;

public class DancerCreateDTO extends RepresentationModel<DancerCreateDTO> {

  private String firstName;
  private String lastName;
  private Integer danceSchoolId;

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public void setDanceSchoolId(Integer danceSchoolId) {
    this.danceSchoolId = danceSchoolId;
  }

  public void setTrainersIds(List<Integer> trainersIds) {
    this.trainersIds = trainersIds;
  }

  private List<Integer> trainersIds;

  @JsonCreator
  public DancerCreateDTO(@JsonProperty("firstName") String firstName,
                         @JsonProperty("lastName") String lastName,
                         @JsonProperty("danceSchoolId") Integer danceSchoolId,
                         @JsonProperty("trainersIds") List<Integer> trainersIds ) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.danceSchoolId = danceSchoolId;
    this.trainersIds = trainersIds;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public Integer getDanceSchoolId() {
    return danceSchoolId;
  }

  public List<Integer> getTrainersIds() {
    return trainersIds;
  }

  @Override
  public boolean equals( Object o ) {
    if ( this == o ) return true;
    if ( o == null || getClass() != o.getClass() ) return false;
    DancerCreateDTO that = ( DancerCreateDTO ) o;
    return Objects.equals( firstName, that.firstName ) && Objects.equals( lastName, that.lastName ) && Objects.equals( danceSchoolId, that.danceSchoolId ) && Objects.equals( trainersIds, that.trainersIds );
  }

  @Override
  public int hashCode() {
    return Objects.hash( firstName, lastName, danceSchoolId, trainersIds );
  }
}
