package cz.cvut.fit.shakhvit.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.hateoas.RepresentationModel;

import java.util.List;
import java.util.Objects;

public class DancerDTO extends RepresentationModel<DancerDTO> {

  private int id;
  private String firstName;
  private String lastName;
  private Integer danceSchoolsId;
  private List<Integer> trainersIds;

  public void setId(int id) {
    this.id = id;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public void setDanceSchoolsId(Integer danceSchoolsId) {
    this.danceSchoolsId = danceSchoolsId;
  }

  public void setTrainersIds(List<Integer> trainersIds) {
    this.trainersIds = trainersIds;
  }

  @JsonCreator
  public DancerDTO(@JsonProperty("id") int id,
                   @JsonProperty("firstName")String firstName,
                   @JsonProperty("lastName")String lastName,
                   @JsonProperty("danceSchoolsId")Integer danceSchoolsId,
                   @JsonProperty("trainersIds")List<Integer> trainersIds ) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.danceSchoolsId = danceSchoolsId;
    this.trainersIds = trainersIds;
  }

  public int getId() {
    return id;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public Integer getDanceSchoolsId() {
    return danceSchoolsId;
  }

  public List<Integer> getTrainersIds() {
    return trainersIds;
  }

  @Override
  public boolean equals( Object o ) {
    if ( this == o ) return true;
    if ( o == null || getClass() != o.getClass() ) return false;
    DancerDTO dancerDTO = ( DancerDTO ) o;
    return id == dancerDTO.id && firstName.equals( dancerDTO.firstName ) && lastName.equals( dancerDTO.lastName ) && Objects.equals( danceSchoolsId, dancerDTO.danceSchoolsId ) && Objects.equals( trainersIds, dancerDTO.trainersIds );
  }

  @Override
  public int hashCode() {
    return Objects.hash( id, firstName, lastName, danceSchoolsId, trainersIds );
  }

  @Override
  public String toString() {
    String res = "ID: " + id + ", first name: " + firstName + ", last name: " + lastName;
    if(danceSchoolsId!=null)
      res += ", dance school id: " + danceSchoolsId;
    if(trainersIds!=null) {
      res += ", trainers ids: [";
      for(int i = 0; i < trainersIds.size(); ++i )
        res += trainersIds.get(i) + (i == trainersIds.size() - 1 ? "" : ", ");
      res += "]"; 
    }
    return res;
  }
}
