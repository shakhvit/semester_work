package cz.cvut.fit.shakhvit.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.hateoas.RepresentationModel;

import java.util.List;
import java.util.Objects;

public class TrainerCreateDTO extends RepresentationModel<TrainerCreateDTO> {

  private String firstName;
  private String lastName;
  private List<Integer> danceSchoolsIds;

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public void setDanceSchoolsIds(List<Integer> danceSchoolsIds) {
    this.danceSchoolsIds = danceSchoolsIds;
  }

  public void setDancersIds(List<Integer> dancersIds) {
    this.dancersIds = dancersIds;
  }

  private List<Integer> dancersIds;

  @JsonCreator
  public TrainerCreateDTO( @JsonProperty("firstName")String firstName,
                           @JsonProperty("lastName")String lastName,
                           @JsonProperty("danceSchoolsIds")List<Integer> danceSchoolsIds,
                           @JsonProperty("dancersIds") List<Integer> dancersIds ) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.danceSchoolsIds = danceSchoolsIds;
    this.dancersIds = dancersIds;
  }

  public String getLastName() {
    return lastName;
  }

  public String getFirstName() {
    return firstName;
  }

  public List<Integer> getDancersIds() {
    return dancersIds;
  }

  public List<Integer> getDanceSchoolsIds() {
    return danceSchoolsIds;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    TrainerCreateDTO that = (TrainerCreateDTO) o;
    return firstName.equals(that.firstName) && lastName.equals(that.lastName) && Objects.equals(danceSchoolsIds, that.danceSchoolsIds) && Objects.equals(dancersIds, that.dancersIds);
  }

  @Override
  public int hashCode() {
    return Objects.hash(firstName, lastName, danceSchoolsIds, dancersIds);
  }
}
