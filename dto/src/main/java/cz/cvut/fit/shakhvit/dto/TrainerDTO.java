package cz.cvut.fit.shakhvit.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;

public class TrainerDTO extends RepresentationModel<TrainerDTO> {

  private int id;
  private String firstName;
  private String lastName;
  private List<Integer> danceSchoolsIds;
  private List<Integer> dancersIds;

  @JsonCreator
  public TrainerDTO( @JsonProperty("id")int id,
                     @JsonProperty("firstName")String firstName,
                     @JsonProperty("lastName")String lastName,
                     @JsonProperty("danceSchoolsIds")List<Integer> danceSchoolsIds,
                     @JsonProperty("dancersIds")List<Integer> dancersIds ) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.danceSchoolsIds = danceSchoolsIds;
    this.dancersIds = dancersIds;
  }

  public List<Integer> getDancersIds() {
    return dancersIds;
  }

  public int getId() {
    return id;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setId(int id) {
    this.id = id;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public void setDanceSchoolsIds(List<Integer> danceSchoolsIds) {
    this.danceSchoolsIds = danceSchoolsIds;
  }

  public void setDancersIds(List<Integer> dancersIds) {
    this.dancersIds = dancersIds;
  }

  public String getLastName() {
    return lastName;
  }

  public List<Integer> getDanceSchoolsIds() {
    return danceSchoolsIds;
  }

  @Override
  public boolean equals( Object o ) {
    if ( this == o ) return true;
    if ( o == null || getClass() != o.getClass() ) return false;
    TrainerDTO that = ( TrainerDTO ) o;
    return id == that.id && firstName.equals( that.firstName ) && lastName.equals( that.lastName ) && Objects.equals( danceSchoolsIds, that.danceSchoolsIds ) && Objects.equals( dancersIds, that.dancersIds );
  }

  @Override
  public int hashCode() {
    return Objects.hash( id, firstName, lastName, danceSchoolsIds, dancersIds );
  }

  @Override
  public String toString() {
    String res = "ID: " + id + ", first name: " + firstName + ", last name: " + lastName;
    if(danceSchoolsIds!=null){
      res += ", dance schools ids: [";
      for(int i = 0; i < danceSchoolsIds.size(); ++i)
        res += danceSchoolsIds.get(i) + (i == danceSchoolsIds.size() - 1 ? "" : ", ");
      res += "]";
    }
    if(dancersIds!=null) {
      res += ", dancers ids: [";
      for(int i = 0; i < dancersIds.size(); ++i )
        res += dancersIds.get(i) + (i == dancersIds.size() - 1 ? "" : ", ");
      res += "]";
    }
    return res;
  }
}
