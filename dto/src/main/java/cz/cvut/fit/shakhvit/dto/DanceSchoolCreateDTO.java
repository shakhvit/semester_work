package cz.cvut.fit.shakhvit.dto;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.hateoas.RepresentationModel;

import java.util.List;
import java.util.Objects;

public class DanceSchoolCreateDTO extends RepresentationModel<DanceSchoolCreateDTO> {

  private String name; // name of dance school. Dance schools must have unique names.
  private int numberOfHalls; // you cannot create dance school without dance halls. That's why it should be not null.
  private List<Integer> dancersIds;

  public void setName(String name) {
    this.name = name;
  }

  public void setNumberOfHalls(int numberOfHalls) {
    this.numberOfHalls = numberOfHalls;
  }

  public void setDancersIds(List<Integer> dancersIds) {
    this.dancersIds = dancersIds;
  }

  public void setTrainersIds(List<Integer> trainersIds) {
    this.trainersIds = trainersIds;
  }

  private List<Integer> trainersIds;

  @JsonCreator
  public DanceSchoolCreateDTO(@JsonProperty("name") String name,
                              @JsonProperty("numberOfHalls") int numberOfHalls,
                              @JsonProperty("dancersIds") List<Integer> dancersIds,
                              @JsonProperty("trainersIds") List<Integer> trainersIds ) {
    this.name = name;
    this.numberOfHalls = numberOfHalls;
    this.dancersIds = dancersIds;
    this.trainersIds = trainersIds;
  }

  public List<Integer> getTrainersIds() {
    return trainersIds;
  }

  public List<Integer> getDancersIds() {
    return dancersIds;
  }

  public String getName() {
    return name;
  }

  public int getNumberOfHalls() {
    return numberOfHalls;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    DanceSchoolCreateDTO that = (DanceSchoolCreateDTO) o;
    return numberOfHalls == that.numberOfHalls && name.equals(that.name) && Objects.equals(dancersIds, that.dancersIds) && Objects.equals(trainersIds, that.trainersIds);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, numberOfHalls, dancersIds, trainersIds);
  }
}
