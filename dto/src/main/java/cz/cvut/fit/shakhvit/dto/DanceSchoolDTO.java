package cz.cvut.fit.shakhvit.dto;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.hateoas.RepresentationModel;

import java.util.List;
import java.util.Objects;

public class DanceSchoolDTO extends  RepresentationModel<DanceSchoolDTO> {

  private int id;
  private String name; // name of dance school. Dance schools must have unique names.
  private int numberOfHalls; // you cannot create dance school without dance halls. That's why it should be not null.
  private List<Integer> dancersIds;
  private List<Integer> trainersIds;

  public void setId(int id) {
    this.id = id;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setNumberOfHalls(int numberOfHalls) {
    this.numberOfHalls = numberOfHalls;
  }

  public void setDancersIds(List<Integer> dancersIds) {
    this.dancersIds = dancersIds;
  }

  public void setTrainersIds(List<Integer> trainersIds) {
    this.trainersIds = trainersIds;
  }

  @JsonCreator
  public DanceSchoolDTO( @JsonProperty("id")int id,
                         @JsonProperty("name")String name,
                         @JsonProperty("numberOfHalls")int numberOfHalls,
                         @JsonProperty("dancersIds")List<Integer> dancersIds,
                         @JsonProperty("trainersIds")List<Integer> trainersIds ) {
    this.id = id;
    this.name = name;
    this.numberOfHalls = numberOfHalls;
    this.dancersIds = dancersIds;
    this.trainersIds = trainersIds;
  }

  public List<Integer> getTrainersIds() {
    return trainersIds;
  }

  public List<Integer> getDancersIds() { return dancersIds; }

  public int getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public int getNumberOfHalls() {
    return numberOfHalls;
  }

  @Override
  public boolean equals( Object o ) {
    if ( this == o ) return true;
    if ( o == null || getClass() != o.getClass() ) return false;
    DanceSchoolDTO that = ( DanceSchoolDTO ) o;
    return id == that.id && numberOfHalls == that.numberOfHalls && name.equals( that.name ) && Objects.equals( dancersIds, that.dancersIds ) && Objects.equals( trainersIds, that.trainersIds );
  }

  @Override
  public int hashCode() {
    return Objects.hash( id, name, numberOfHalls, dancersIds, trainersIds );
  }

  @Override
  public String toString() {
    String res = "ID: " + id + ", name: " + name + ", number of halls: " + numberOfHalls;
    if(trainersIds!=null){
      res += ", trainers ids: [";
      for(int i = 0; i < trainersIds.size(); ++i)
        res += trainersIds.get(i) + (i == trainersIds.size() - 1 ? "" : ", ");
      res += "]";
    }
    if(dancersIds!=null) {
      res += ", dancers ids: [";
      for(int i = 0; i < dancersIds.size(); ++i )
        res += dancersIds.get(i) + (i == dancersIds.size() - 1 ? "" : ", ");
      res += "]";
    }
    return res;
  }
}

